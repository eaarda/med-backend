from django.template import Context
from django.core.mail import EmailMessage
from django.template.loader import get_template, render_to_string
from django.conf import settings
from django.core.mail import send_mail


def WELCOME_MAIL(user):
    subject = u"Medikaport'a Hoşgeldiniz"
    context = Context({'name':user.first_name})
    html_version = 'welcome_email.html'
    html_message = render_to_string(html_version, {'context':context})
    message = EmailMessage(subject, html_message, settings.DEFAULT_FROM_EMAIL, [user.email])
    message.content_subtype = 'html'
    return message.send()


def CONFIRMED_MAIL(user):
    subject = u"Medikaport Hesabınız Onaylandı"
    link = '%s/account/login' % (settings.SITE_HOST)
    context = Context({'name':user.first_name, 'link': link})
    html_version = 'confirmed_email.html'
    html_message = render_to_string(html_version, {'context':context})
    message = EmailMessage(subject,html_message, settings.DEFAULT_FROM_EMAIL, [user.email])
    message.content_subtype = 'html'
    return message.send()


def RESET_PASSWORD_MAIL(user):
    subject = u"Şifremi Unuttum"
    link = '%s/account/reset-password?hash=%s' % (settings.SITE_HOST, user.password)
    context = Context({'name': user.first_name, 'link': link})
    html_version = 'reset_password_email.html'
    html_message = render_to_string(html_version, {'context':context})
    message = EmailMessage(subject,html_message, settings.DEFAULT_FROM_EMAIL, [user.email])
    message.content_subtype = 'html'
    return message.send()


def RANDOM_PASSWORD_MAIL(user, password):
    subject = u"Medikaport'a Hoşgeldiniz"
    link = '%s/account/login' % (settings.SITE_HOST)
    context = Context({'name':user.first_name, 'password':password, 'link':link})
    html_version = 'random_password_mail.html'
    html_message = render_to_string(html_version, {'context':context})
    message = EmailMessage(subject,html_message, settings.DEFAULT_FROM_EMAIL, [user.email])
    message.content_subtype = 'html'
    return message.send()


def NEW_USER_NOTIFICATION_MAIL(user):
    subject = u"Yeni Kullanıcı"
    link = '%s/accounts/edit/%s' % (settings.ADMIN_HOST, user.id)
    context = Context({'email':user.email, 'company':user.company.name, 'company_no':user.company.company_no, 'link':link })
    html_version = 'new_user_notification_email.html'
    html_message = render_to_string(html_version, {'context':context})
    message = EmailMessage(subject,html_message, settings.DEFAULT_FROM_EMAIL, [settings.NOTIFICATION_EMAIL, settings.CUSTOMER_SERVICE_EMAIL])
    message.content_subtype = 'html'
    return message.send()
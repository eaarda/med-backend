from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (    RegisterViewSet, LoginViewSet, UserAuthorizationViewSet, ChangePasswordView,
                        CompanyViewSet, AddressViewSet, MeViewSet, ForgotPasswordView, ResetPasswordView )

router = DefaultRouter()
router.register(r'companies', CompanyViewSet)
router.register(r'addresses', AddressViewSet)

urlpatterns = [
    path('', include(router.urls)),
    
    path('register/', RegisterViewSet.as_view(), name='register'),
    path('login/', LoginViewSet.as_view(), name='login'),

    path('me/', MeViewSet.as_view(), name="me"),
    path('authorization/', UserAuthorizationViewSet.as_view(), name='authorization'),
    path('change-password/', ChangePasswordView.as_view(), name="change_password"),
    path('forgot-password/', ForgotPasswordView.as_view(), name="forgot_password"),
    path('reset-password/', ResetPasswordView.as_view(), name="reset_password"),
  
]
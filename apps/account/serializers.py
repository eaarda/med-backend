from drf_writable_nested.serializers import WritableNestedModelSerializer
from drf_writable_nested.mixins import UniqueFieldsMixin
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
import json

from .utils import WELCOME_MAIL, NEW_USER_NOTIFICATION_MAIL

from apps.base.serializers import BaseSerializer
from apps.product.serializers import CategorySerializer

from apps.account.models import Account, Address, Company
from apps.listing.models import ListingStatus


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(style={'input_type': 'password'}, max_length=128)
    class Meta:
        model = Account
        fields = ('email', 'password')


class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True,validators=[UniqueValidator(queryset=Account.objects.all(), lookup='iexact' )])
    password = serializers.CharField(style={'input_type': 'password'}, required=True, validators=[validate_password])
    repeat_password = serializers.CharField(style={'input_type': 'password'}, write_only=True, required=True)

    first_name = serializers.CharField(required=True, write_only=True)
    last_name = serializers.CharField(required=True, write_only=True)
    company = serializers.CharField(required=True, write_only=True)
    phone = serializers.CharField(required=True, write_only=True, validators=[UniqueValidator(queryset=Account.objects.all(), lookup='iexact' )])
    company_phone = serializers.CharField(required=True, write_only=True, validators=[UniqueValidator(queryset=Company.objects.all(), lookup='iexact')])
    company_no = serializers.CharField(required=True, write_only=True, validators=[UniqueValidator(queryset=Company.objects.all(), lookup='iexact')])
    short_name = serializers.CharField(required=True, write_only=True, min_length=5, max_length=8, validators=[UniqueValidator(queryset=Company.objects.all(), lookup='iexact')])

    mms_approval = serializers.BooleanField(required=False, write_only=True, default=False)

    def validate(self, attrs):
        if attrs['password'] != attrs['repeat_password']:
            raise serializers.ValidationError({"password": f"Passwords must match.."})
        return attrs

    def create(self, validated_data):
        user = Account.objects.create(  email=validated_data['email'].lower(),
                                        first_name=validated_data['first_name'],
                                        last_name=validated_data['last_name'],
                                        phone=validated_data['phone'],
                                    )
        Company.objects.create( account=user,
                                name=validated_data['company'],
                                company_no=validated_data['company_no'],
                                company_phone=validated_data['company_phone'],
                                short_name=validated_data['short_name'],
                                mms_approval=validated_data['mms_approval'])
        user.set_password(validated_data['password'])
        user.save()

        # Welcome Mail
        WELCOME_MAIL(user)

        # Admin Notification Mail
        NEW_USER_NOTIFICATION_MAIL(user)

        return validated_data


class ChangePasswordSerializer(serializers.Serializer):

    old_password = serializers.CharField(style={'input_type': 'password'}, required=True)
    new_password = serializers.CharField(style={'input_type': 'password'}, required=True, validators=[validate_password])


class ResetPasswordSerializer(serializers.Serializer):
    new_password1 = serializers.CharField(style={'input_type': 'password'}, required=True)
    new_password2 = serializers.CharField(style={'input_type': 'password'}, required=True)
    hash = serializers.CharField(required=True)

class CompanySerializer(WritableNestedModelSerializer, UniqueFieldsMixin):
    
    class Meta:
        model = Company
        fields = (  'id', 'name', 'short_name',
                    'company_no', 'company_phone', 'iban', 'bank_account_name', 'commission_rate',
                    'tax_number', 'tax_office', 'mms_approval', 'seller_note',
                    'billing_address', 'billing_district', 'billing_city')


class AccountSerializer(WritableNestedModelSerializer):
    company = CompanySerializer()
    class Meta:
        model = Account
        fields = (  'id', 'email', 'first_name', 'last_name', 'phone', 'company')
        read_only_fields = ('email',)

class CompanyLiteSerializer(BaseSerializer):
    categories = serializers.SerializerMethodField()
    class Meta:
        model = Company
        fields = (  'id', 'short_name', 'categories', 'seller_note')
    
    def get_categories(self,obj):

        all_categories = []
        categories = []

        for listing in obj.listings.filter(status=ListingStatus.ONLINE):
            if listing.product.category:
                all_categories.append(listing.product.category)
        categories_data = CategorySerializer(all_categories, many=True).data

        # Remove duplicate categories
        c = { each['name'] : each for each in categories_data }.values()
        categories = json.loads(json.dumps(list(c)))

        # Seller categories product count
        product_count = {}
        for category in categories:
            product_count[category['name']] = obj.listings.filter(product__category__name=category['name']).count()
        for category in categories:
            category['product_count'] = product_count[category['name']]


        return categories


class AddressSerializer(BaseSerializer):
    city_name = serializers.StringRelatedField(source='city')
    district_name = serializers.StringRelatedField(source='district')

    class Meta:
        model = Address
        fields = (  'id', 'title', 'name', 'phone', 'email', 'address', 
                    'district','district_name', 'city', 'city_name')
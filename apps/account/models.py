from statistics import mode
import uuid
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from .managers import AccountManager
from django.core.validators import MinLengthValidator

from apps.base.models import BaseModel
from apps.common.models import City, District


class Account(AbstractBaseUser):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)

    email = models.CharField(max_length=50, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=50)

    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now_add=True)
    verification_date = models.DateTimeField(null=True, blank=True)

    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_superadmin = models.BooleanField(default=False)

    is_banned = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    description = models.CharField(max_length=255, blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = AccountManager()

    class Meta:
        db_table = 'accounts'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, add_label):
        return True


class Company(BaseModel):

    account = models.OneToOneField(Account, on_delete=models.CASCADE, related_name='company')
    
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=8, validators=[MinLengthValidator(5)])
    company_no = models.CharField(max_length=25, unique=True)
    company_phone = models.CharField(max_length=50)

    tax_number = models.CharField(max_length=11, blank=True, null=True)
    tax_office = models.CharField(max_length=255, blank=True, null=True)

    billing_address = models.CharField(max_length=255, blank=True, null=True)
    billing_district = models.ForeignKey(District, on_delete=models.SET_NULL, blank=True, null=True, related_name='companies_billing')
    billing_city = models.ForeignKey(City, on_delete=models.SET_NULL, blank=True, null=True, related_name='companies_billing')

    commission_rate = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    iban = models.CharField(max_length=26, blank=True, null=True)
    bank_account_name = models.CharField(max_length=255, blank=True, null=True)

    mms_approval = models.BooleanField(default=False, help_text='İleti Yönetim Sistemi Onayı')

    seller_note = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'companies'


class Address(BaseModel):

    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='addresses')

    title = models.CharField(max_length=255)

    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=255)

    address = models.CharField(max_length=255)
    district = models.ForeignKey(District, on_delete=models.PROTECT)
    city = models.ForeignKey(City, on_delete=models.PROTECT)

    class Meta:
        db_table = 'addresses'
        ordering = ['-created_at']
    
    def __str__(self):
        return self.title
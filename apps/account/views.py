from rest_framework import serializers, status
from rest_framework.generics import CreateAPIView, UpdateAPIView, RetrieveUpdateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth import authenticate, login
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework_simplejwt.tokens import RefreshToken
from django.shortcuts import get_object_or_404
from django.db.models import Q

from apps.base.views import BaseModelViewSet, BaseReadOnlyModelViewSet
from apps.cart.models import Cart, Favourite
from .models import Account, Address, Company
from .serializers import AddressSerializer, CompanyLiteSerializer, CompanySerializer, RegisterSerializer, LoginSerializer, ChangePasswordSerializer, AccountSerializer, ResetPasswordSerializer
from apps.cart.serializers import CartSerializer, FavouriteSerializer
from apps.order.models import Order, OrderItemStatusType, PaymentStatusType
from .utils import RESET_PASSWORD_MAIL


class RegisterViewSet(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = RegisterSerializer


class LoginViewSet(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self, request):

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.data['email']
        password = serializer.data['password']

        # User Control
        if not Account.objects.filter(email=email).exists():
            raise serializers.ValidationError({'invalid':['Invalid username or password']})

        user = authenticate(email=email, password=password)
        if not user or user.is_anonymous:
            try:
                account = Account.objects.get(email=email)
                if account.check_password(password):
                    return Response({
                        'is_active': False,
                    }, status=status.HTTP_403_FORBIDDEN)
            except Account.DoesNotExist:
                account = None
                
            raise serializers.ValidationError({'invalid':['Invalid username or password']})
            
        refresh = RefreshToken.for_user(user)
        cart = CartSerializer(Cart.objects.filter(account=user), context = {'request':request}, many=True)
        favourites =FavouriteSerializer(Favourite.objects.filter(account=user), context = {'request':request}, many=True)

        if user.company.billing_city:
            billing_city = user.company.billing_city.id
        else:
            billing_city = None
        if user.company.billing_district:
            billing_district = user.company.billing_district.id
        else:
            billing_district = None

        data = {
            'is_active': user.is_active,
            'is_banned': user.is_banned,
            'is_admin': user.is_admin,
            'refresh':str(refresh),
            'access':str(refresh.access_token),
            'email':user.email,
            'first_name':user.first_name,
            'last_name':user.last_name,
            'phone': user.phone,
            'company': {
                'id': user.company.id,
                'name': user.company.name,
                'short_name': user.company.short_name,
                'company_no': user.company.company_no,
                'company_phone': user.company.company_phone,
                'tax_number': user.company.tax_number,
                'tax_office': user.company.tax_office,
                'billing_address': user.company.billing_address,
                'billing_district': billing_district,
                'billing_city': billing_city,
                'iban': user.company.iban,
                'bank_account_name': user.company.bank_account_name,
            },
            'cart': cart.data,
            'favourites': favourites.data,
        }

        login(request,user)
        return Response(data, status.HTTP_200_OK)


class UserAuthorizationViewSet(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):

        cart = CartSerializer(Cart.objects.filter(account=request.user), context = {'request':request}, many=True)
        favourites =FavouriteSerializer(Favourite.objects.filter(account=request.user), context = {'request':request}, many=True)
        
        if request.user.company.billing_city:
            billing_city = request.user.company.billing_city.id
        else:
            billing_city = None
        if request.user.company.billing_district:
            billing_district = request.user.company.billing_district.id
        else:
            billing_district = None

        data = {
            'user':request.user.id,
            'is_active': request.user.is_active,
            'is_banned': request.user.is_banned,
            'is_admin':request.user.is_admin,
            'email':request.user.email,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'phone': request.user.phone,
            'company': {
                'id': request.user.company.id,
                'name': request.user.company.name,
                'short_name': request.user.company.short_name,
                'company_no': request.user.company.company_no,
                'company_phone': request.user.company.company_phone,
                'tax_number': request.user.company.tax_number,
                'tax_office': request.user.company.tax_office,
                'billing_address': request.user.company.billing_address,
                'billing_district': billing_district,
                'billing_city': billing_city,
                'iban': request.user.company.iban,
                'bank_account_name': request.user.company.bank_account_name,
                'commission_rate': request.user.company.commission_rate
            },
            'cart': cart.data,
            'favourites': favourites.data,
            'passive_notifications': {
                'new_order': Order.objects.filter(order_items__listing__company=request.user.company).filter(Q(order_items__status=OrderItemStatusType.AWAITING) | Q(order_items__status=OrderItemStatusType.CONFIRMED)).filter(order_items__payment_status=PaymentStatusType.PAID).distinct().count(),
                'notifications':[]
            }
        }

        return Response(data, status.HTTP_200_OK)


class CompanyViewSet(BaseReadOnlyModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanyLiteSerializer


class MeViewSet(RetrieveUpdateAPIView):
    queryset = Account.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = AccountSerializer

    def get(self,request,format=None):
        account = get_object_or_404(Account, id = request.user.id)
        serializer = AccountSerializer(account)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    def patch(self,request,format=None):
        account = get_object_or_404(Account, id = request.user.id)
        serializer = AccountSerializer(account, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddressViewSet(BaseModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer

    def get_queryset(self):
        return self.request.user.addresses.all()

    def perform_create(self, serializer):
        return serializer.save(account = self.request.user)


class ChangePasswordView(UpdateAPIView):
    serializer_class = ChangePasswordSerializer

    def update(self,request, *args, **kwargs):

        user = self.request.user
        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            if not user.check_password(serializer.data.get("old_password")):
                raise serializers.ValidationError({'old_password':['Current password is incorrect']})

            user.set_password(serializer.data.get("new_password"))
            user.save()
            
            return Response({'message': ['Password was successfully updated!']}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ForgotPasswordView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        if 'email' in request.data:
            email = request.data['email']
            user = Account.objects.get(email=email)
            # # Reset Password email
            RESET_PASSWORD_MAIL(user)

            return Response({"message":"Sıfırlama linki gönderildi."},status=status.HTTP_200_OK)

        raise serializers.ValidationError({'email':["Bu alan zorunlu."]})


class ResetPasswordView(APIView):
    permission_classes = [AllowAny]
    serializer_class = ResetPasswordSerializer

    def post(self, request):

        # Request data control
        serializer = ResetPasswordSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = get_object_or_404(Account, password = request.data['hash'])

            if request.data['new_password1'] != request.data['new_password2']:
                raise serializers.ValidationError({'password':['Şifre alanları eşleşmedi.']})

        # Password Validation control
        errors = dict() 
        try:
            validate_password(password=request.data['new_password1'])
        except ValidationError as e:
            errors['password'] = list(e.messages)
        if errors:
             raise serializers.ValidationError(errors)
        
        # Password Reset
        user.set_password(request.data.get("new_password1"))
        user.save()

        return Response({"message": "Şifre sıfırlandı."}, status=status.HTTP_200_OK)
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Account, Company


class CompanyInline(admin.TabularInline):
    model = Company

class AccountAdmin(UserAdmin):
    list_display = ['get_company', 'first_name', 'last_name', 'email', 'get_tax_number', 'phone', 'is_active', 'date_joined', 'last_login']
    list_display_links = ['get_company', 'first_name', 'last_name', 'email']
    readonly_fields = ['last_login', 'date_joined']
    ordering = ['-date_joined']
    inlines = (CompanyInline,)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = (
        (None, {
            'fields': ('email', 'first_name', 'last_name', 'phone', 'is_active',)
        }),
    )
    add_fieldsets = (
        (None, {
            'fields': ('email', 'password1', 'password2', 'first_name', 'last_name', 'phone', 'is_active')
        }),
    )

    def get_company(self,obj):
        return obj.company.name
    def get_tax_number(self,obj):
        return obj.company.tax_number
    get_company.short_description = 'company'
    get_tax_number.short_description = 'tax_number'


admin.site.register(Account, AccountAdmin)

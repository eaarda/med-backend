from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import CategoryViewSet, ProductViewSet, BrandViewSet, SearchViewSet


router = DefaultRouter()
router.register(r'categories', CategoryViewSet)
router.register(r'products', ProductViewSet)
router.register(r'brands', BrandViewSet)


urlpatterns = [
    path('', include(router.urls)),

    path('search/', SearchViewSet.as_view(), name="search"),
]
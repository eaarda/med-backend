from django.contrib import admin
from django import forms
from django.utils.html import format_html
from apps.common.models import Image
from mptt.admin import DraggableMPTTAdmin
from .models import Category, Brand, Product, ProductImages


class CategoryAdmin(DraggableMPTTAdmin):
    list_display = ['indented_title']


# class CategoryAdmin(DraggableMPTTAdmin):
#     mptt_indent_field = "name"
#     list_display = ('tree_actions', 'indented_title',
#                     'related_products_count', 'related_products_cumulative_count')
#     list_display_links = ('indented_title',)

#     def get_queryset(self, request):
#         qs = super().get_queryset(request)

#         # Add cumulative product count
#         qs = Category.objects.add_related_count(
#                 qs,
#                 Product,
#                 'category',
#                 'products_cumulative_count',
#                 cumulative=True)

#         # Add non cumulative product count
#         qs = Category.objects.add_related_count(qs,
#                  Product,
#                  'categories',
#                  'products_count',
#                  cumulative=False)
#         return qs

#     def related_products_count(self, instance):
#         return instance.products_count
#     related_products_count.short_description = 'Related products (for this specific category)'

#     def related_products_cumulative_count(self, instance):
#         return instance.products_cumulative_count
#     related_products_cumulative_count.short_description = 'Related products (in tree)'


class BrandAdmin(admin.ModelAdmin):
    list_display = ['name']


class ProductImagesInline(admin.TabularInline):
    model = ProductImages
    extra = 1

class ProductAdmin(admin.ModelAdmin):
    inlines = (ProductImagesInline, )
    list_display = [ 'name', 'category', 'price', 'updated_at', 'active']


admin.site.register(Brand, BrandAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
from unicodedata import category
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.db.models import Min
from apps.base.serializers import BaseSerializer
from apps.common.serializers import ImageSerializer, ImageLiteSerializer
from .models import Category, CategoryStatus, Product, Brand, BrandStatus
from apps.listing.models import ListingStatus
from .utils import GET_CATEGORY_TREE

class CategorySerializer(BaseSerializer):
    children = serializers.SerializerMethodField()
    parent_name = serializers.StringRelatedField(source='parent')
    parent = serializers.SerializerMethodField()
    image_url = ImageSerializer(source='image', read_only=True, required=False)
    product_count = serializers.SerializerMethodField()

    def get_parent(self,obj):
        return GET_CATEGORY_TREE(obj)

    def get_children(self, obj):
        if obj.children:
            return CategorySerializer(obj.children, many=True).data
        else:
            return []
    
    def get_product_count(self, obj):
        return obj.products.count()
    
    class Meta:
        model = Category
        fields = ('id', 'active', 'created_at', 'name', 'parent_name', 'product_count', 'parent', 'description', 'image', 'image_url', 'is_featured', 'status','children' )


class ProductSerializer(BaseSerializer):
    status = serializers.CharField(source='get_status_display', required=False)
    barcode = serializers.CharField(required=True, validators=[UniqueValidator(queryset=Product.objects.all(), lookup='iexact')])
    seller_count = serializers.SerializerMethodField()
    minimum_price = serializers.SerializerMethodField()
    # category = serializers.SerializerMethodField()
    category_display = serializers.SerializerMethodField()
    # brand = serializers.SerializerMethodField()
    brand_display = serializers.SerializerMethodField()

    image_url = ImageSerializer(source='image', read_only=True, required=False)

    images = ImageLiteSerializer(many=True, allow_null=True, required=False)
    images_url = ImageSerializer(source='images', many=True, read_only=True, required=False)

    def get_seller_count(self, obj):
        return obj.listings.filter(status=ListingStatus.ONLINE).count()

    def get_minimum_price(self, obj):    
        return obj.listings.filter(status=ListingStatus.ONLINE).aggregate(min_price=Min('price'))['min_price']
    
    # def get_category(self, obj):
    #     if obj.category and obj.category.status == CategoryStatus.APPROVED:
    #         return obj.category.id
    #     return None

    def get_category_display(self, obj):
        if obj.category and obj.category.status == CategoryStatus.APPROVED:
            return GET_CATEGORY_TREE(obj.category)
        return None
    
    # def get_brand(self, obj):
    #     if obj.brand and obj.brand.status == BrandStatus.APPROVED:
    #         return obj.brand.id
    #     return None
    
    def get_brand_display(self, obj):
        if obj.brand and obj.brand.status == BrandStatus.APPROVED:
                return BrandSerializer(obj.brand).data
        return None

    class Meta:
        model = Product
        fields = (  'id', 'active', 'status', 'name', 'barcode',
                    'image', 'image_url', 'images', 'images_url',
                    'price', 'seller_count', 'minimum_price', 
                    'brand', 'brand_display', 'category', 'category_display',
                    'hic_code', 'parent_name', 'model', 'pts_code',
                    'description', 'is_featured')


class BrandSerializer(BaseSerializer):
    product_count = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        fields = '__all__'
    
    def get_product_count(self, obj):
        return obj.products.count()
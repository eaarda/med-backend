import json
from django.core.management.base import BaseCommand, CommandError
from pathlib import Path
from backend.utils import printProgressBar
from apps.product.models import Brand


class Command(BaseCommand):

    def handle(self, *args, **options):
        WORKING_DIR = Path(__file__).resolve().parent

        with open("{}/brands.json".format(WORKING_DIR), encoding="utf8") as json_file:
            data = json.load(json_file)
            brands = data["brands"]
            
            index = 0
            total = len(brands)

            print("Total:", total)

            printProgressBar(index, total, prefix = 'Loading brands:', suffix = 'Complete', length = 50)
            
            for brand in brands:
                Brand.objects.get_or_create(name=brand['name'])

                index += 1                
                printProgressBar(index, total, prefix = 'Loading brands:', suffix = 'Complete', length = 50)

        self.stdout.write(self.style.SUCCESS('Brands loaded successfully!'))

import json
import random
from decimal import *
from django.core.management.base import BaseCommand, CommandError
from backend.utils import printProgressBar
from apps.product.models import Product
from apps.account.models import Company
from apps.listing.models import Listing, ListingStatus


class Command(BaseCommand):

    def handle(self, *args, **options):
        products = Product.objects.all()

        index = 0
        total = products.count()

        printProgressBar(index, total, prefix = 'Listings:', suffix = 'Complete', length = 50)

        for product in products:
            count = random.randint(10, 30)
            
            for i in range(count):
                company = Company.objects.order_by('?').all()[:1][0]
                count = Listing.objects.filter(product=product, company=company).all().count()

                if count == 0:
                    discount = random.randint(10, 50) / 100

                    Listing.objects.create(
                        product=product, 
                        company=company,
                        status = ListingStatus.ONLINE,
                        price = product.price * Decimal(discount),
                        quantity = random.randint(10, 100),
                        buying_price = product.price,
                    )

            index += 1                
            printProgressBar(index, total, prefix = 'Listings:', suffix = 'Complete', length = 50)

        self.stdout.write(self.style.SUCCESS('Listings created successfully!'))

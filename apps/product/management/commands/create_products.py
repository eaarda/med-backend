import json
import csv
from venv import create
from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from urllib.request import urlopen, Request
from tempfile import NamedTemporaryFile
from pathlib import Path
from apps.common.models import Image, MediaKind
from backend.utils import printProgressBar
from apps.product.models import Category, Brand, Product, ProductStatus


class Command(BaseCommand):

    # Returns dictreader's length
    # def get_csv_length(self, csv_file):
        

    def handle(self, *args, **options):
        WORKING_DIR = Path(__file__).resolve().parent

        with open("{}/products.tsv".format(WORKING_DIR), newline='') as file:
            file_reader = csv.DictReader(file, delimiter='\t')
            
            # print("Total:", len(list(file_reader)))
            
            for line in file_reader:
                name = line['ÜRÜN ADI']
                parent_name = line['ÜRÜN ÜTS ADI']
                barcode = line['ÜTS KODU / BARKOD']
                pts_code = line['ÜTS KODU / BARKOD']
                brand_name = line['MARKA']
                model = line['MODEL']
                parent_category_name = line['KATEGORİ']
                child_category_name = line['ALT KATEGORİ']
                gmdn = line['GMDN KODU']
                hic_code = line['SUT KODU']
                image_filename = line['GÖRSEL NUMARA']+'.jpg'
                
                if name: 
                    print("Product:", name)
                    brand, created = Brand.objects.get_or_create(name=brand_name)
                    if created:
                        print("Brand created:", brand_name)

                    parent_category, created = Category.objects.get_or_create(name=parent_category_name)
                    if created:
                        print("Parent category created:", parent_category_name)
                    
                    child_category, created = Category.objects.get_or_create(parent=parent_category, name=child_category_name)
                    if created:
                        print("Child category created:", child_category_name)
                    
                    product, created = Product.objects.get_or_create(
                        name=name, 
                        parent_name=parent_name, 
                        brand=brand, 
                        category=child_category, 
                        barcode=barcode, 
                        pts_code=pts_code, 
                        model=model, 
                        gmdn=gmdn, 
                        hic_code=hic_code,
                        status=ProductStatus.APPROVED
                    )

                    if created: 
                        image_file = open("{}/images/{}".format(WORKING_DIR, image_filename), "rb")
                        image = Image.objects.create(kind=MediaKind.PRODUCT)
                        image.image.save(f"image_{product.id}", File(image_file))
                        
                        product.image = image                
                        product.save()            
                    else:
                        product.status = ProductStatus.APPROVED

        return

        brand = Brand.objects.first()

        with open("{}/products.json".format(WORKING_DIR), encoding="utf8") as json_file:
            data = json.load(json_file)
            
            index = 0
            total = 0

            categories = data["categories"]
            for category in categories:
                children = category["children"]
                for child in children:
                    products = child["products"]
                    total += len(products)

            print("Total Products:", total)

            printProgressBar(index, total, prefix = 'Loading products:', suffix = 'Complete', length = 50)
            
            for parent in categories:
                category, created = Category.objects.get_or_create(name=parent['title'])

                children = parent["children"]
                for child in children:
                    child_category, created = Category.objects.get_or_create(parent=category, name=child['title'])

                    products = child["products"]
                    for product in products:
                        try:
                            item, created = Product.objects.get_or_create(
                                category=child_category, 
                                name=product['title'], 
                                brand=brand,
                                price=product['price']
                            )

                            if not created and not item.image and not '/home/kalitel1/public_html' in product['image']:
                                # print(index+1, "/", total, item.name, product['image'])
                                req = Request(product['image'])
                                req.add_header('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36')

                                img_temp = NamedTemporaryFile(delete=True)
                                img_temp.write(urlopen(req).read())
                                img_temp.flush()

                                image = Image.objects.create(kind=MediaKind.PRODUCT)
                                image.image.save(f"image_{item.id}", File(img_temp))
                                item.image = image
                                item.save()
                        except Exception as e:
                            item.image = None
                            item.save()
                            print("Error:", e)

                        index += 1                
                        printProgressBar(index, total, prefix = 'Loading products:', suffix = 'Complete', length = 50)

        self.stdout.write(self.style.SUCCESS('Images loaded successfully!'))

import json
from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from urllib.request import urlopen, Request
from tempfile import NamedTemporaryFile
from pathlib import Path
from apps.common.models import Image, MediaKind
from backend.utils import printProgressBar
from apps.account.models import Account, Company


class Command(BaseCommand):

    def handle(self, *args, **options):
        WORKING_DIR = Path(__file__).resolve().parent

        with open("{}/accounts.txt".format(WORKING_DIR), encoding="utf8") as data_file:
            index = 0
            total = sum(1 for line in data_file)

            printProgressBar(index, total, prefix = 'Loading accounts:', suffix = 'Complete', length = 50)

            data_file.seek(0)
            for line in data_file:
                company = line.strip()

                if Company.objects.filter(name=company).all().count() == 0:
                    req = Request('https://randomuser.me/api?nat=tr')
                    req.add_header('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36')

                    response = urlopen(req)
                    data = json.load(response)
                    data = data['results'][0]

                    email = data['email']
                    # username = email.replace('@example.com', '')
                    first_name = data['name']['first']
                    last_name = data['name']['last']
                    phone = data['phone']
                    company_no = data['login']['md5'][:11]
                    password = '12345678!'

                    count = Account.objects.filter(email=email).all().count()
                    if count == 0:
                        account = Account(
                            email=email, 
                            first_name = first_name,
                            last_name = last_name,
                            phone = phone,
                            is_active = True,
                        )
                        account.set_password(password),
                        account.save()
                        
                        Company.objects.create(account=account, name=company, company_no=company_no)
            
                index += 1                
                printProgressBar(index, total, prefix = 'Loading accounts:', suffix = 'Complete', length = 50)

        self.stdout.write(self.style.SUCCESS('Accounts loaded successfully!'))

import json
from django.core.management.base import BaseCommand, CommandError
from pathlib import Path
from backend.utils import printProgressBar
from apps.product.models import Category


class Command(BaseCommand):

    def handle(self, *args, **options):
        WORKING_DIR = Path(__file__).resolve().parent

        with open("{}/categories.json".format(WORKING_DIR), encoding="utf8") as json_file:
            data = json.load(json_file)
            categories = data["categories"]
            
            index = 0
            total = len(categories)

            print("Total:", total)

            printProgressBar(index, total, prefix = 'Loading categories:', suffix = 'Complete', length = 50)
            
            for category in categories:
                parent, created = Category.objects.get_or_create(name=category['name'])

                children = category["children"]
                for child in children:
                    child_category, created = Category.objects.get_or_create(parent=parent, name=child['name'])

                index += 1                
                printProgressBar(index, total, prefix = 'Loading categories:', suffix = 'Complete', length = 50)

        self.stdout.write(self.style.SUCCESS('Categories loaded successfully!'))

from admin.product.serializers import BrandSerializer
from apps.product.utils import QUERYSET_CREATOR
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from backend.utils import serialize_data
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.db.models import Q

from apps.product.serializers import CategorySerializer, ProductSerializer
from apps.product.models import Brand, BrandStatus, Category, CategoryStatus, Product, ProductOwner, ProductStatus

from apps.base.views import BaseReadOnlyModelViewSet
from apps.listing.models import Listing, ListingStatus
from apps.listing.serializers import ListingSerializer


class CategoryViewSet(BaseReadOnlyModelViewSet):
    """
    API endpoint that allows categories to be viewed.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    @action(detail=True, url_path="products")
    def products(self, request, pk):
        category = self.get_object()
        context = {
            'request': self.request
        }
        result = Product.objects.filter(category=category).all().order_by('?')[:10]        

        page = self.paginate_queryset(result)
        if page is not None:
            serializer = ProductSerializer(page, many=True, context=context)
            return self.get_paginated_response(serializer.data)

        serializer = ProductSerializer(result, many=True, context=context)
        return Response(serializer.data)    


class ProductViewSet(BaseReadOnlyModelViewSet):
    """
    API endpoint that allows products to be viewed.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def list(self, *args, **kwargs):
        return QUERYSET_CREATOR(self, self.queryset, self.request)
    
    # def get_object(self):
    #     obj = super().get_object()
    #     if obj.status == ProductStatus.APPROVED:
    #         return obj
    #     else:
    #         raise Http404()


    @action(detail=False, methods=['get', 'post'], url_path='recommendation')
    def recommendation(self, request):

        if request.method == 'GET':
            products = Product.objects.filter(id__in=ProductOwner.objects.filter(account=request.user).values_list('product_id', flat=True))
            serializer = ProductSerializer(products, many=True)
            data = {
                'total' : len(products),
                'results' : serializer.data
            }
            return Response(data, 200)

        if request.method == 'POST':
            if 'brand' in request.data and request.data['brand'] != '' and request.data['brand'] != None and request.data['brand'] != ' ':
                try:
                    brand = Brand.objects.get(name=request.data['brand'])
                except:
                    brand = Brand.objects.create(name=request.data['brand'], status=BrandStatus.WAITING_APPROVAL)
                request.data['brand'] = brand.id
            else:
                request.data['brand'] = None


            if 'category' in request.data and request.data['category'] != '' and request.data['category'] != None and request.data['category'] != ' ':
                try:
                    category = Category.objects.get(name=request.data['category'])
                except:
                    category = Category.objects.create(name=request.data['category'], status=CategoryStatus.WAITING_APPROVAL)
                request.data['category'] = category.id
            else:
                request.data['category'] = None


            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                ProductOwner.objects.create(product=serializer.instance, account=request.user)
                return Response(serializer.data, 201)


    @action(detail=False, url_path="explore")
    def explore(self, request):
        result = Product.objects.filter(Q(active=True) & Q(status=ProductStatus.APPROVED)).filter(Q(listings__isnull=False) & Q(listings__status=ListingStatus.ONLINE)).order_by('?')[:18]
        return serialize_data(self, result)

    @action(detail=False, url_path="top-selling")
    def top_selling(self, request):
        result = Product.objects.filter(Q(active=True) & Q(status=ProductStatus.APPROVED)).filter(Q(listings__isnull=False) & Q(listings__status=ListingStatus.ONLINE)).order_by('?')[:12]
        return serialize_data(self, result)

    @action(detail=False, url_path="last-week-top-selling")
    def last_week_top_selling(self, request):
        result = Product.objects.filter(Q(active=True) & Q(status=ProductStatus.APPROVED)).filter(Q(listings__isnull=False) & Q(listings__status=ListingStatus.ONLINE)).order_by('?')[:12]
        return serialize_data(self, result)
    
    @action(detail=False, methods=['post'], url_path="recently-visited")
    def recently_visited(self, request):
        data = []
        for item in request.data:
            product = ProductSerializer(Product.objects.get(id=item['product_id']), context = {'request':request}).data
            data.append(product)
        return Response(data, 200)

    
    @action(detail=True, methods=['post'], url_path="add-listing")
    def add_listing(self, request, pk):
        product = self.get_object()
        company = self.request.user.company

        request.data['company'] = company.id
        request.data['product'] = product.id
        if product.status == ProductStatus.WAITING_APPROVAL or request.data['is_secondhand'] == True :
            request.data['status'] = ListingStatus.WAITING

        listings = Listing.objects.filter(product=product, company=company, is_secondhand=False).count()
        if listings > 0 and request.data['is_secondhand'] == False:
            return Response({'message': 'You already have a listing for this product'}, 400)
        
        serializer = ListingSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        
        return Response(serializer.data, 201)

    
    @action(detail=False, methods=['delete'], url_path="delete-listing/(?P<listing_pk>\d+)")
    def delete_listing(self, request, listing_pk):
        company = self.request.user.company
        listing = get_object_or_404(Listing, company=company, pk=listing_pk)
        listing.delete()
        return Response({'message': 'listing deleted'}, 204)
    
    @action(detail=False, methods=['put'], url_path="update-listing/(?P<listing_pk>\d+)")
    def update_listing(self, request, listing_pk):
        company = self.request.user.company
        listing = get_object_or_404(Listing, company=company, pk=listing_pk)

        if 'images' in request.data:
            for image in request.data['images']:
                if image['id'] not in listing.images.all().values_list('id', flat=True):
                    request.data['status'] = ListingStatus.WAITING

        request.data['company'] = company.id
        request.data['product'] = listing.product.id

        serializer = ListingSerializer(listing, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, 200)


class BrandViewSet(BaseReadOnlyModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

    
class SearchViewSet(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        result = {
                'products': {
                    'items':[],
                    'count': None
                },
                'categories': {
                    'items': [],
                    'count': None
                },
                'brands': {
                    'items': [],
                    'count': None
                }
            }

        search = request.GET.get('search')
        if search:
            products = Product.objects.filter(status=ProductStatus.APPROVED).filter(name__icontains=search)[:5]
            result['products']['items'] = ProductSerializer(products, many=True, context={'request': request}).data
            result['products']['count'] = Product.objects.filter(status=ProductStatus.APPROVED).filter(name__icontains=search).count()

            categories = Category.objects.filter(active=True).filter(name__icontains=search)[:2]
            result['categories']['items'] = CategorySerializer(categories, many=True, context={'request': request}).data
            result['categories']['count']= Category.objects.filter(active=True).filter(name__icontains=search).count()

            brands = Brand.objects.filter(status=BrandStatus.APPROVED).filter(name__icontains=search)[:1]
            result['brands']['items'] = BrandSerializer(brands, many=True, context={'request': request}).data
            result['brands']['count'] = Brand.objects.filter(status=BrandStatus.APPROVED).filter(name__icontains=search).count()

        return Response(result,200)
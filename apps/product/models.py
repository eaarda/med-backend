from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from apps.base.models import BaseModel
from apps.common.models import Image
from apps.account.models import Account
from django.utils.translation import gettext_lazy as _


class ProductStatus(models.TextChoices):    
    WAITING_APPROVAL = 'waiting_approval', _('Onay Bekliyor')
    APPROVED = 'approved', _('Onaylandı')
    REJECTED = 'rejected', _('Reddedildi')
    WAITING_IMAGE = 'waiting_image', _('Resim Bekliyor')


class BrandStatus(models.TextChoices):
    WAITING_APPROVAL = 'waiting_approval', _('Onay Bekliyor')
    APPROVED = 'approved', _('Onaylandı')
    REJECTED = 'rejected', _('Reddedildi')

class CategoryStatus(models.TextChoices):
    WAITING_APPROVAL = 'waiting_approval', _('Onay Bekliyor')
    APPROVED = 'approved', _('Onaylandı')
    REJECTED = 'rejected', _('Reddedildi')


class Category(MPTTModel):
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    name = models.CharField(max_length=100, unique=True, db_index=True)
    description = models.TextField(blank=True)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, related_name='categories', blank=True, null=True)
    is_featured = models.BooleanField(default=False)
    status = models.CharField(max_length=20, choices=CategoryStatus.choices, default=CategoryStatus.APPROVED)

    class Meta:
        db_table = 'categories'
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Brand(BaseModel):
    name = models.CharField(max_length=200, unique=True, db_index=True)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, related_name='brands', blank=True, null=True)
    is_featured = models.BooleanField(default=False)
    status = models.CharField(max_length=20, choices=BrandStatus.choices, default=BrandStatus.APPROVED)

    
    class Meta:
        db_table = 'brands'

    def __str__(self):
        return self.name


class Product(BaseModel):
    name = models.CharField(max_length=255, db_index=True)
    description = models.TextField(blank=True, db_index=True)
    barcode = models.CharField(max_length=50, unique=True, db_index=True, blank=True, null=True)
    gmdn = models.CharField(max_length=5, blank=True, null=True, db_index=True)
    brand = models.ForeignKey(Brand, on_delete=models.SET_NULL, related_name='products', null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, related_name='products', null=True)
    price = models.DecimalField(default=0.0, max_digits=10, decimal_places=2, blank=True, null=True)

    image = models.ForeignKey(Image, on_delete=models.SET_NULL, related_name='product_images', blank=True, null=True)
    images = models.ManyToManyField(Image, through='ProductImages', related_name='products')

    hic_code = models.CharField(max_length=255, blank=True, null=True, db_index=True, help_text=' Sağlık Uygulama Tebliği Kodu')
    parent_name = models.CharField(max_length=255, blank=True, null=True, db_index=True)
    model = models.CharField(max_length=255, blank=True, null=True, db_index=True)
    pts_code = models.CharField(max_length=50, blank=True, null=True, db_index=True)

    status = models.CharField(max_length=20, choices=ProductStatus.choices, default=ProductStatus.WAITING_APPROVAL)
    is_featured = models.BooleanField(default=False)

    class Meta:
        db_table = 'products'
        ordering = ['-created_at']

    def __str__(self):
        return self.name


class ProductImages(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    class Meta:
        db_table = "product_images"
        verbose_name_plural = "Product Images"


class ProductOwner(BaseModel):

    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='owner')
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='owner')

    class Meta:
        db_table = "product_owners"
        verbose_name_plural = "Product Owners"
import json
from apps.product.models import Category, Brand, ProductStatus
from django.db.models.functions import Lower
from django.db.models import Q


def GET_CATEGORY_TREE(category):
    
    return {
        'id': category.id,
        'name': category.name,
        'parent': GET_CATEGORY_TREE(category.parent) if category.parent else None,
    }


def REMOVE_DUPLICATE_DATA(all_data):

    unique_data = { each['name'] : each for each in all_data }.values()
    data = json.loads(json.dumps(list(unique_data)))
    return data


def QUERYSET_CREATOR(self, queryset, request):
    from backend.utils import serialize_data_with_filters, serialize_data

    category_id = request.GET.get('category_id')
    brand_id = request.GET.get('brand_id')
    search = request.GET.get('search')
    ordering = request.GET.get('ordering')
    status = request.GET.get('status')
    full_search = request.GET.get('full_search')

    if not category_id and not brand_id and not search and not ordering and not status and not full_search:
        return serialize_data(self, queryset)
    
    if full_search:
        queryset = queryset.filter( Q(name__icontains=full_search) |
                                    Q(barcode__icontains=full_search) | 
                                    Q(model__icontains=full_search) |
                                    Q(hic_code__icontains=full_search) |
                                    Q(pts_code__icontains=full_search) )

                                            
        return serialize_data(self, queryset)


    if category_id:
        category_list = category_id.split(u',')
        try:
            if len(category_list) == 1:
                queryset = queryset.filter(category__in=Category.objects.get(id=category_list[0]).get_descendants(include_self=True))
            else:
                queryset = queryset.filter(**{'category__id__in': category_list})
        except:
            queryset = queryset

    if brand_id:
        brand_list = brand_id.split(u',')
        try:
            queryset = queryset.filter(**{'brand__id__in': brand_list,})
        except:
            queryset = queryset

    if search:
        queryset = queryset.filter( Q(name__icontains=search) |
                                    Q(barcode__icontains=search) | 
                                    Q(model__icontains=search) |
                                    Q(hic_code__icontains=search) |
                                    Q(pts_code__icontains=search) )

    if ordering:
        queryset = queryset.order_by(ordering)

    if status:
        queryset = queryset.filter(status=status)

    
    queryset = queryset.filter(status=ProductStatus.APPROVED)
    return serialize_data_with_filters(self, queryset)
from django.db import models
from apps.base.models import BaseModel

from apps.account.models import Account
from apps.listing.models import Listing


class Favourite(BaseModel):

    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='favourites')
    listing = models.ForeignKey(Listing, models.CASCADE, related_name='favourites')

    class Meta:
        db_table = 'favourites'
    
    def __str__(self):
        return self.listing.product.name


class Cart(BaseModel):
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='carts')
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name='carts')
    expiry = models.DateField(blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    quantity = models.PositiveIntegerField(default=1)

    class Meta:
        db_table = 'carts'
        ordering = ['-created_at']

    def __str__(self):
        return self.account.email
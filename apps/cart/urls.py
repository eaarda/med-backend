from django.urls import path, include
from rest_framework import routers

from .views import FavouriteViewSet, CartViewSet


router = routers.DefaultRouter()
router.register(r"favourites", FavouriteViewSet)
router.register(r"cart", CartViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
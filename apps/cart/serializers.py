from rest_framework import serializers
from rest_framework.response import Response

from apps.base.serializers import BaseSerializer
from .models import Favourite, Cart
from apps.listing.models import Listing
from apps.listing.serializers import ListingSerializer


class FavouriteSerializer(BaseSerializer):

    class Meta:
        model = Favourite
        fields = ('id', 'listing')
    
    def to_representation(self, instance):
        data = super(FavouriteSerializer, self).to_representation(instance)
        listing = ListingSerializer(Listing.objects.get(id=data['listing']), context = {'request':self.context['request']}).data

        if 'id' in data:
            id = data['id']
        else:
            favourite = Favourite.objects.get(listing=data['listing'])
            id = favourite.id
        return {
            "id": id,
            "listing": listing
        }
    

class CartSerializer(BaseSerializer):
    listing = ListingSerializer()
    class Meta:
        model = Cart
        fields = ('id', 'listing', 'expiry', 'price', 'quantity')
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import serializers

from apps.base.views import BaseModelViewSet

from .models import Favourite, Cart
from .serializers import FavouriteSerializer, CartSerializer


class FavouriteViewSet(BaseModelViewSet):
    queryset = Favourite.objects.all()
    serializer_class = FavouriteSerializer
    http_method_names = ['get', 'post']

    def list(self, request):
        serializer = self.serializer_class(self.get_queryset().filter(account=request.user), context = {'request':request}, many=True)
        return Response({"results": serializer.data})
    
    def create(self, request):
        data = {"detail": "Method \"POST\" not allowed."}
        return Response(data,status=status.HTTP_405_METHOD_NOT_ALLOWED)

    
    @action(methods=['post'], detail=False, url_path="add")
    def add(self, request, pk=None):

        for data in request.data:
            try:
                favourite, created = Favourite.objects.get_or_create(account=self.request.user, listing_id=data['listing'])
            except:
                pass

        serializer = self.serializer_class(self.get_queryset().filter(account=request.user), context = {'request':request}, many=True)
        return Response({"results": serializer.data})

    
    @action(methods=['post'], detail=False, url_path="delete", url_name="delete_favourites")
    def delete_favourites(self, request, pk=None):

        for data in request.data:
            
            try:
                favourite = Favourite.objects.get(account=request.user, id=data)
                favourite.delete()
            except:
                pass
        
        serializer = self.serializer_class(self.get_queryset().filter(account=request.user), context = {'request':request}, many=True)
        return Response({"results": serializer.data})


class CartViewSet(BaseModelViewSet):

    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    http_method_names = ['get', 'post']

    def create(self, request):
        data = {"detail": "Method \"POST\" not allowed."}
        return Response(data,status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def list(self, request):
        errors = []
        account_cart = self.get_queryset().filter(account=request.user)
        for item in account_cart:
            max = item.listing.max_quantity
            min = item.listing.min_quantity
            quantity = item.listing.quantity

            if max != 0 and max < item.quantity:
                errors.append({"item": item.listing.id, "quantity": item.quantity, "message": "Quantity must be less than or equal to " + str(max)})
                
            if min != 0 and min > item.quantity:
                errors.append({"item": item.listing.id, "quantity": item.quantity, "message": "Quantity must be greater than or equal to " + str(min)})
                
            if quantity < item.quantity:
                errors.append({"item": item.listing.id, "message": "Stock is not enough"})
            
            if len(errors) > 0:
                for i in errors:
                    item.delete()
            
        serializer = self.serializer_class(self.get_queryset().filter(account=request.user), context = {'request':request}, many=True)
        return Response({"results": serializer.data, "errors": errors})
    
    
    @action(methods=['post'], detail=False, url_path="add")
    def add(self, request, pk=None):

        for data in request.data:
            try:
                account_cart, created = Cart.objects.get_or_create(account=request.user, listing_id=data['item'])
                account_cart.quantity = data['quantity']
                account_cart.save()
                
            except:
                pass

        serializer = self.serializer_class(self.get_queryset().filter(account=request.user), context = {'request':request}, many=True)
        return Response({"results": serializer.data})
    

    @action(methods=['post'], detail=False, url_path="delete", url_name="delete")
    def delete(self, request, pk=None):

        for data in request.data:
            try:
                cart = Cart.objects.get(account=request.user, id=data)
                cart.delete()
            except:
                pass

        serializer = self.serializer_class(self.get_queryset().filter(account=request.user), context = {'request':request}, many=True)
        return Response({"results": serializer.data})
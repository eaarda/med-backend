from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer
from apps.common.models import Setting
from apps.product.serializers import ProductSerializer
from apps.base.serializers import BaseSerializer
from .models import Listing
from apps.common.serializers import ImageSerializer, ImageLiteSerializer


class ListingSerializer(BaseSerializer, WritableNestedModelSerializer):
    company_display = serializers.SerializerMethodField()
    product_display = serializers.SerializerMethodField()
    earning = serializers.SerializerMethodField()

    image_url = ImageSerializer(source='image', read_only=True, required=False)
    images = ImageLiteSerializer(many=True, allow_null=True, required=False)
    images_url = ImageSerializer(source='images', many=True, read_only=True, required=False)

    class Meta:
        model = Listing
        fields = (  'id', 'created_at', 'product', 'product_display', 'product_code', 'company', 'company_display',
                    'status', 'expiry', 'price', 'buying_price', 'earning', 'shipment_kind',
                    'quantity', 'description', 'max_quantity', 'min_quantity',
                    'is_secondhand', 'condition', 'usage_time', 'image', 'image_url', 'images', 'images_url')
    
    def get_company_display(self, obj):
        return {
            "id": obj.company.id,
            "name":obj.company.short_name,
            "seller_note": obj.company.seller_note,
        }
    
    def get_product_display(self, obj):
        serializer = ProductSerializer(obj.product).data
        return serializer
    
    def get_earning(self, obj):
        if obj.company.commission_rate:
            commission_rate = obj.company.commission_rate
        else:
            setting = Setting.objects.first()
            commission_rate = setting.commission_rate
        
        earning = obj.price * (100 - commission_rate) / 100

        return {
            'commission_rate': commission_rate,
            'price': earning
        }
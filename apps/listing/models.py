from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.base.models import BaseModel
from apps.account.models import Company
from apps.product.models import Product
from apps.common.models import Image


class ListingStatus(models.TextChoices):    
    ONLINE = 'online'
    OFFLINE = 'offline'
    WAITING = 'waiting'
    WAITING_IMAGE = 'waiting_image', _('Resim Bekliyor')

class ShipmentKind(models.TextChoices):
    REVERSE_CHARGE = 'reverse_charge', _('Alıcı Ödemeli')
    WITHOUT_CHARGE = 'without_charge', _('Satıcı Ödemeli')

class ConditionType(models.TextChoices):
    VERY_WORN = "very_worn", _("Çok yıpranmış")
    MIDDLE = "middle", _("Orta")
    GOOD = "good", _("İyi")


class Listing(BaseModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='listings')
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='listings')

    status = models.CharField(max_length=20, choices=ListingStatus.choices, default=ListingStatus.ONLINE)
    expiry = models.DateField(blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    quantity = models.PositiveIntegerField()
    description = models.TextField(blank=True, null=True)
    max_quantity = models.PositiveIntegerField(default=0, blank=True, null=True)
    min_quantity = models.PositiveIntegerField(default=0, blank=True, null=True)
    buying_price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    product_code = models.CharField(max_length=50, blank=True, null=True)

    shipment_kind = models.CharField(max_length=20, choices=ShipmentKind.choices, default=ShipmentKind.REVERSE_CHARGE)

    is_secondhand = models.BooleanField(default=False)
    image = models.ForeignKey(Image, on_delete=models.CASCADE, related_name='listing_image', blank=True, null=True)
    images = models.ManyToManyField(Image, through='ListingImages', related_name='listings')
    condition =  models.CharField(max_length=20, choices=ConditionType.choices, default=ConditionType.MIDDLE)
    usage_time = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'listings'
        ordering = ['price']

    def __str__(self):
        return '{} - {}'.format(self.company.name, self.product.name)


class ListingImages(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)

    class Meta:
        db_table = "listing_images"
        verbose_name_plural = "Listing Images"
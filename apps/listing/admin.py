from django.contrib import admin
from .models import Listing


class ListingAdmin(admin.ModelAdmin):
    list_display = [ 'product', 'get_company', 'status', 'price', 'quantity' ]
    list_display_links = ['product', 'get_company', ]

    def get_company(self,obj):
        return obj.company.name


admin.site.register(Listing, ListingAdmin)
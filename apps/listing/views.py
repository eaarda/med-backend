from rest_framework import viewsets
from apps.base.views import BaseReadOnlyModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Listing
from .serializers import ListingSerializer

from apps.common.models import ErrorReport
from apps.common.serializers import ErrorReportSerializer


class ListingViewSet(BaseReadOnlyModelViewSet):
    queryset = Listing.objects.all()
    serializer_class = ListingSerializer

    ordering_case_insensitive_fields = [
        'product__name', '-product__name'
    ]

    @action(detail=True, methods=['post'], url_path="error-report")
    def error_report(self, request, pk):
        
        request.data['listing'] = self.get_object().id
        request.data['account'] = self.request.user.id

        try:
            report = ErrorReport.objects.get(listing=self.get_object(), account=self.request.user, active=True)
        except ErrorReport.DoesNotExist:
            serializer = ErrorReportSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
            return Response(serializer.data, 201)
        
        return Response({'message': 'report already exists'}, 400)
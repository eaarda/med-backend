from django.contrib import admin
from django import forms
from .models import Image, Setting, SocialMedia


class ImageForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update(
            {'multiple': True })


class ImageAdmin(admin.ModelAdmin):
    form = ImageForm

    def save_model(self, request, obj, form, change):
        
        images = request.FILES.getlist('image')
        for image in images:
            Image.objects.create(image=image, kind=request.POST['kind'])


class SocialMediaInline(admin.TabularInline):
    model = SocialMedia

class SettingAdmin(admin.ModelAdmin):
    list_display = ['title']
    inlines = [SocialMediaInline, ]

    def has_add_permission(self, request):
        return not Setting.objects.exists()


admin.site.register(Image , ImageAdmin)
admin.site.register(Setting, SettingAdmin )
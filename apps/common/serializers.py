from apps.base.serializers import BaseSerializer
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer

from .models import CargoCompany, Image, Setting, SocialMedia, City, District, Slider, SliderItem, Post, ErrorReport, Document


class DistrictSerializer(BaseSerializer, WritableNestedModelSerializer):
    
    class Meta:
        model = District
        fields = ('id', 'name')


class CitySerializer(BaseSerializer, WritableNestedModelSerializer):
    district = serializers.SerializerMethodField()
    class Meta:
        model = City
        fields = ('id',  'name', 'district')
    
    def get_district(self, obj):
        if obj.districts:
            return DistrictSerializer(obj.districts, many=True).data
        else:
            return []


class ImageSerializer(BaseSerializer): 
    class Meta:
        model = Image
        fields = ('id', 'kind', 'name', 'account', 'image', 'created_at')
    
    def to_representation(self, instance):

        data = super(ImageSerializer, self).to_representation(instance)
        return {
            "id" : data['id'],
            "kind" : data['kind'],
            "name": data['name'],
            "account": data['account'],
            "image": instance.image.url,
            "created_at" : data['created_at']
        }


class ImageLiteSerializer(BaseSerializer):
    
    class Meta:
        model = Image
        fields = ['id']


class SocialMediaSerializer(BaseSerializer, WritableNestedModelSerializer):
    
    class Meta:
        model = SocialMedia
        fields = ('id', 'icon', 'url')


class SliderItemSerializer(BaseSerializer, WritableNestedModelSerializer):
    image_url = ImageSerializer(source='image', read_only=True, required=False)
    class Meta:
        model = SliderItem
        fields = ('id', 'image', 'image_url', 'url', 'order', 'title', 'subtitle', 'button_title' )


class SliderSerializer(BaseSerializer, WritableNestedModelSerializer):
    items = SliderItemSerializer(many=True, required=False)
    class Meta:
        model = Slider
        fields = ('id', 'name', 'description', 'items')


class SettingSerializer(BaseSerializer, WritableNestedModelSerializer):
    social_medias = SocialMediaSerializer(many=True, required=False, allow_null=True)
    sliders = SliderSerializer(many=True, required=False, allow_null=True)
    image_url = ImageSerializer(source='image', read_only=True, required=False)
    favicon_url = ImageSerializer(source='favicon', read_only=True, required=False)

    class Meta:
        model = Setting
        fields = (  'id', 'title', 'motto', 'seperator', 'phone', 'email',
                    'social_medias', 'sliders',
                    'image', 'image_url', 'favicon', 'favicon_url',
                    'commission_rate', 'maintenance', 'maintenance_message')


class PostSerializer(BaseSerializer):
    image_url = ImageSerializer(source='image', read_only=True, required=False)

    class Meta:
        model = Post
        fields = '__all__'


class CargoCompanySerializer(BaseSerializer):
    image_url = ImageSerializer(source='image', read_only=True, required=False)

    class Meta:
        model = CargoCompany
        fields = ('id', 'name', 'url', 'image', 'image_url')


class ErrorReportSerializer(BaseSerializer):
    class Meta:
        model = ErrorReport
        fields = ('id', 'account', 'listing', 'description')


class DocumentSerializer(BaseSerializer):
    
    class Meta:
        model = Document
        fields = ('id', 'created_at', 'account', 'kind', 'document', 'name')
    
    def to_representation(self, instance):

        data = super(DocumentSerializer, self).to_representation(instance)
        return {
            "id" : data['id'],
            "created_at" : data['created_at'],
            "account": data['account'],
            "kind": data['kind'],
            "document": instance.document.url,
            "name": data['name']
        }
from django.core.management.base import BaseCommand
from apps.common.management.commands.common_mock import CommonMock
from apps.common.models import Setting


class Command(BaseCommand):

    def handle(self, *args, **options):

        CommonMock().execute()

        if not Setting.objects.exists():
            setting, created =  Setting.objects.get_or_create(  title='Medikaport',
                                                                motto='Medikaport',
                                                                seperator='-',
                                                                commission_rate=10)
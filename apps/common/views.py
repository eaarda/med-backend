from apps.base.views import BaseModelViewSet, BaseReadOnlyModelViewSet
from rest_framework.response import Response
from rest_framework.views import APIView 
from rest_framework.permissions import AllowAny
from django.db.models import Q

from .models import CargoCompany, Image, MediaKind, PostStatus, Setting, City, District, Post, Document
from .serializers import CargoCompanySerializer, ImageSerializer, SettingSerializer, CitySerializer, DistrictSerializer, PostSerializer, DocumentSerializer
from apps.order.models import Order, OrderItemStatusType, PaymentStatusType

class CityViewSet(BaseReadOnlyModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer


class DistrictViewSet(BaseReadOnlyModelViewSet):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer


class ImageViewSet(BaseModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer

    def create(self, request, *args, **kwargs):
        data = {
            "results":[],
            "errors":[]
        }

        images = self.request.FILES.getlist('image')
        for image in images:
            try:
                request.data['image'] = image
                request.data['name'] = image.name
                request.data['account'] = self.request.user.id
                serializer = self.serializer_class(data=request.data)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                data['results'].append(serializer.data)
            except:
                data['errors'].append(image.name)

        return Response(data, 200)


class SettingViewSet(BaseReadOnlyModelViewSet):
    queryset = Setting.objects.all()
    serializer_class = SettingSerializer
    permission_classes = [AllowAny]

    def list(self, request):
        serializer = self.serializer_class(self.queryset.filter().first())
        return Response(serializer.data)


class PostViewSet(BaseReadOnlyModelViewSet):
    queryset = Post.objects.filter(status=PostStatus.PUBLISHED)
    serializer_class = PostSerializer
    lookup_field = 'slug'


class CargoCompanyViewSet(BaseReadOnlyModelViewSet):
    queryset = CargoCompany.objects.filter(active=True)
    serializer_class = CargoCompanySerializer


class DocumentViewSet(BaseModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer

    def create(self, request, *args, **kwargs):

        document = self.request.FILES['document']
        request.data['document'] = document
        request.data['name'] = document.name
        request.data['account'] = request.user.id
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, 200)


class NotificationView(APIView):
    def get(self, request):
        data = {
                'new_order': Order.objects.filter(order_items__listing__company=request.user.company).filter(Q(order_items__status=OrderItemStatusType.AWAITING) | Q(order_items__status=OrderItemStatusType.CONFIRMED)).filter(order_items__payment_status=PaymentStatusType.PAID).distinct().count(),
                'notifications':[]
            }
        return Response(data, 200)
from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.base.utils import CREATE_SLUG

from apps.base.models import BaseModel


class MediaKind(models.TextChoices):
    OTHER = 'other', _('Diğer')
    PRODUCT = 'product', _('Ürün')
    BRAND = 'brand', _('Marka')
    CATEGORY = 'category', _('Kategori')
    BLOG = 'blog', _('Blog')
    SLIDER = 'slider', _('Slider')
    BANNER = 'banner', _('Banner')
    LOGO = 'logo', _('Logo')
    SECONDHAND = 'secondhand', _('İkinci El')

class DocumentKind(models.TextChoices):
    OTHER = 'other', _('Diğer')
    INVOICE = 'invoice', _('Fatura')
    CONTRACT = 'contract', _('Sözleşme')


class PostKind(models.TextChoices):
    PAGE = 'page', _('Sayfa')
    BLOG = 'blog', _('Blog')


class PostStatus(models.TextChoices):
    DRAFT = 'draft', _('Taslak')
    PUBLISHED = 'published', _('Yayınlandı')


def upload_image(instance, filename):
    if instance.kind == MediaKind.CATEGORY:
        return '/'.join(['images', 'categories', filename])
    return '/'.join(['images', instance.kind + 's', filename])

def upload_document(instance, filename):
    return '/'.join(['documents', instance.kind + 's', filename])


class City(BaseModel):

    name = models.CharField(max_length=100, db_index=True)

    class Meta:
        db_table = 'cities'
        ordering = ['name']

    def __str__(self):
        return self.name


class District(BaseModel):

    name = models.CharField(max_length=100, db_index=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='districts')

    class Meta:
        db_table = 'districts'
        ordering = ['name']

    def __str__(self):
        return self.name


class Image(models.Model):

    account = models.ForeignKey('account.account', on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    kind = models.CharField(max_length=20, choices=MediaKind.choices, default=MediaKind.OTHER)
    image = models.ImageField('Image', upload_to=upload_image)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'images'


class Setting(BaseModel):

    title = models.CharField(max_length=255)
    motto = models.CharField(max_length=255)
    seperator = models.CharField(max_length=3)

    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=255, blank=True, null=True)

    image = models.ForeignKey(Image, on_delete=models.CASCADE, related_name='settings', blank=True, null=True)
    favicon = models.ForeignKey(Image, on_delete=models.CASCADE, related_name='settings_icon', blank=True, null=True) 

    commission_rate = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    maintenance = models.BooleanField(default=False)
    maintenance_message = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'settings'
        verbose_name_plural = 'Settings'


class SocialMedia(BaseModel):

    setting = models.ForeignKey(Setting, on_delete=models.CASCADE, related_name='social_medias')
    icon = models.CharField(max_length=255, blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'social_medias'


class Slider(BaseModel):

    setting = models.ForeignKey(Setting, on_delete=models.CASCADE, related_name='sliders')

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'sliders'


class SliderItem(BaseModel):

    slider = models.ForeignKey(Slider, on_delete=models.CASCADE, related_name='slider_items')

    image = models.ForeignKey(Image, on_delete=models.CASCADE, related_name='slider_items')
    url = models.CharField(max_length=255, blank=True, null=True)
    order = models.PositiveSmallIntegerField(default=999, blank=True, null=True)

    title = models.CharField(max_length=255, blank=True, null=True)
    subtitle = models.CharField(max_length=255, blank=True, null=True)
    button_title = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        db_table = 'slider_items'


class Post(BaseModel):

    kind = models.CharField(max_length=20, choices=PostKind.choices, default=PostKind.BLOG)

    title = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True, blank=True, null=True)

    status = models.CharField(max_length=20, choices=PostStatus.choices, default=PostStatus.DRAFT)
    content = models.TextField()

    image = models.ForeignKey(Image, on_delete=models.SET_NULL, related_name='posts', blank=True, null=True)

    class Meta:
        db_table = "posts"
    
    def __str__(self):
        return self.title
    
    def save(self,*args,**kwargs):
        super(Post, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = CREATE_SLUG(self.title)
            self.save()


class CargoCompany(BaseModel):

    name = models.CharField(max_length=255)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, related_name='cargo_companies', blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'cargo_companies'
    
    def __str__(self):
        return self.name


class ErrorReport(BaseModel):

    account = models.ForeignKey('account.account', on_delete=models.SET_NULL, blank=True, null=True, related_name='error_reports')
    listing = models.ForeignKey('listing.listing', on_delete=models.CASCADE, blank=True, null=True, related_name='error_reports')
    description = models.TextField()

    class Meta:
        db_table = 'error_reports'
    
    def __str__(self):
        return '{} : {}'.format(self.listing.product.name, self.description)


class Document(BaseModel):

    document = models.FileField(upload_to=upload_document, max_length=500)
    name = models.CharField(max_length=255, blank=True, null=True)
    account = models.ForeignKey('account.account', on_delete=models.SET_NULL, related_name='invoices', blank=True, null=True)
    kind = models.CharField(max_length=20, choices=DocumentKind.choices, default=DocumentKind.OTHER)

    class Meta:
        db_table = 'documents'
from django.urls import path, include
from rest_framework import routers
from django.conf.urls import url

from .views import ImageViewSet, SettingViewSet, CityViewSet, PostViewSet, CargoCompanyViewSet, NotificationView, DocumentViewSet

router = routers.DefaultRouter()

router.register(r"images", ImageViewSet)
router.register(r"settings", SettingViewSet)
router.register(r"cities", CityViewSet)
router.register(r"posts", PostViewSet)
router.register(r"cargo-companies", CargoCompanyViewSet)
router.register(r"documents", DocumentViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('notifications/', NotificationView.as_view(), name='notifications'),


]
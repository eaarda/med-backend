from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import GetTokenView, paytr_callback


router = DefaultRouter()


urlpatterns = [

    path('get-token/', GetTokenView.as_view(), name='get-token'),
    path('paytr-callback/', paytr_callback, name='paytr-callback'),
    
    path('', include(router.urls)),
]
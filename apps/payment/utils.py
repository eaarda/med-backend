from datetime import datetime
from pathlib import Path
from urllib.request import urlopen, Request
from tempfile import NamedTemporaryFile
import time, base64, hmac, hashlib, requests, json, os, random

from apps.account.models import Address, Company
from apps.listing.models import Listing
from apps.cart.models import Cart
from apps.listing.serializers import ListingSerializer
from apps.order.models import Order
from apps.order.serializers import OrderSerializer
from apps.order.utils import DISTANCE_SELLING_CONTRACT, CONTEXT_DATA, PRE_INFORMATION_FORM


def GET_ENCODED_IMAGE(url):
    req = Request(url)
    req.add_header('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36')

    encoded_img = str(base64.b64encode(urlopen(req).read())).replace('b\'', '').replace('\'', '')
    return encoded_img

def GET_CLIENT_IP(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def GENERATE_ORDER_NO():
        date = str(datetime.now().strftime("%Y%m%d%H%m%S"))
        count = Order.objects.count()
        return "%s%s" % (date, count+1)

def CLEAR_BASKET_ITEMS(data, user):
    for i in data:
        try:
            cart = Cart.objects.get(account=user, listing=i['listing'])
            cart.delete()
        except:
            pass
    return 200

def STOCK_CONTROL(data):
    for i in data['items']:
        try:
            listing = Listing.objects.get(id=i['listing'])
            if i['quantity'] > listing.quantity:
                return False
            if listing.max_quantity != 0 and listing.max_quantity < i['quantity']:
                return False
            if listing.min_quantity != 0 and listing.min_quantity > i['quantity']:
                return False
        except:
            pass

    return True

def STOCK_UPDATE(data):
    for i in data:
        try:
            listing = Listing.objects.get(id=i['listing'])
            listing.quantity -= i['quantity']
            listing.save()
        except:
            pass
    return 200

def CREATE_ORDER_FOR_PAYMENT(self, data, user):

    # Request Data Control
    try:
        delivery = Address.objects.get(id=data['delivery'], account=user)
        for i in data['items']:
            item = Listing.objects.get(id=i['listing'])
        billing_address = user.company.billing_address
        billing_district = user.company.billing_district.name
        billing_city = user.company.billing_city.name
    except:
        return None
    
    order_items = []
    order_total = 0
    for i in data['items']:
        if 'quantity' in i: quantity = i['quantity']
        else: quantity = 1
        listing = Listing.objects.get(id=i['listing'])
        total = listing.price * quantity
        listing_data = ListingSerializer(listing).data
        commission_rate = listing_data['earning']['commission_rate']
        earning = round(total * (100 - commission_rate) / 100, 2)

        encoded_img = GET_ENCODED_IMAGE(listing_data['product_display']['image_url']['image'])

        data = {
            'listing': listing_data['id'],
            'quantity': quantity,
            'price': listing_data['price'],
            'product_name': listing_data['product_display']['name'],
            'product_image': encoded_img,
            'company_id': listing_data['company_display']['id'],
            'company_name': listing_data['company_display']['name'],
            'total': total,
            'commission_rate': commission_rate,
            'earning': earning
        }
        order_items.append(data)
        order_total += total
    
    validated_data={
        'account': user.id,
        'order_no': GENERATE_ORDER_NO(),
        'delivery': delivery.id,
        'delivery_name': delivery.name,
        'delivery_phone': delivery.phone,
        'delivery_email': delivery.email,
        'delivery_address': delivery.address,
        'delivery_district': delivery.district.name,
        'delivery_city': delivery.district.city.name,
        'billing_address': billing_address,
        'billing_district': billing_district,
        'billing_city': billing_city,
        'billing_name': user.company.name,
        'tax_office': user.company.tax_office,
        'tax_number': user.company.tax_number,
        'order_items':order_items,
        'total': order_total
    }

    serializer = OrderSerializer(data=validated_data)
    if serializer.is_valid(raise_exception=True):
        serializer.save()
    
    CLEAR_BASKET_ITEMS(order_items, user)
    STOCK_UPDATE(order_items)

    return serializer.data


def PAYTR_TOKEN_FOR_PAYMENT(request, order):
    merchant_id = os.environ.get('PAYTR_MERCHANT_ID')
    merchant_key = os.environ.get('PAYTR_MERCHANT_KEY').encode('utf-8')
    merchant_salt = os.environ.get('PAYTR_MERCHANT_SALT').encode('utf-8')
    user_ip = GET_CLIENT_IP(request)

    merchant_oid = order['order_no']
    email = request.user.email
    user_name = request.user.first_name + ' ' +  request.user.last_name
    user_address = request.user.company.billing_address
    user_phone = request.user.phone

    payment_amount = str(round(float(order['total'] * 100)))

    basket = []
    for i in order['order_items']:
        listing = Listing.objects.get(id=i['listing'])
        item_data = [listing.product.name, str(listing.price), i['quantity']]
        basket.append(item_data)
    
    user_basket = base64.b64encode(json.dumps(basket).encode())

    currency = 'TL'
    no_installment = '1'
    max_installment = '0'
    test_mode = os.environ.get('PAYTR_TEST_MODE') # Mağaza canlı modda iken test işlem yapmak için 1 gönderilebilir.
    debug_on = '1'
    timeout_limit = '30'
    
    hash_str = merchant_id + user_ip + merchant_oid + email + payment_amount + user_basket.decode() + no_installment + max_installment + currency + test_mode
    paytr_token = base64.b64encode(hmac.new(merchant_key, hash_str.encode() + merchant_salt, hashlib.sha256).digest())

    params = {
        'merchant_id': merchant_id,
        'user_ip': user_ip,
        'merchant_oid': merchant_oid,
        'email': email,
        'payment_amount': payment_amount,
        'paytr_token': paytr_token,
        'user_basket': user_basket,
        'debug_on': debug_on,
        'no_installment':no_installment,
        'max_installment': max_installment,
        'user_name': user_name,
        'user_address': user_address,
        'user_phone': user_phone,
        'merchant_ok_url': "https://app.medikaport.com/profile/orders/buyings/view/{}?new=true".format(order['id']),
        'merchant_fail_url': "https://app.medikaport.com/profile/orders/buyings/{}".format(order['id']),
        'timeout_limit': timeout_limit,
        'currency': currency,
        'test_mode':test_mode
    }

    result = requests.post('https://www.paytr.com/odeme/api/get-token', params)
    res = json.loads(result.text)
    # if res['status'] == 'success':
        # print(res['token'])
    # else:
        # print(result.text)

    return res
    

def CREATE_ORDER_CONTRACTS(order):
    res_data = {
            'distance_selling_contracts': [],
            'pre_information_forms': [],
        }

    user = {
        "name": order['delivery_name'],
        "address" : '{}  {}/{}'.format(order['delivery_address'], order['delivery_district'], order['delivery_city']),
        "phone": order['delivery_phone'],
        "email": order['delivery_email']
    }

    group_by_order_items_company = {}
    for i in order['order_items']:
        if i['company_id'] not in group_by_order_items_company:
            group_by_order_items_company[i['company_id']] = []
        group_by_order_items_company[i['company_id']].append(i)
    
    for seller in group_by_order_items_company:
        listings = []
        company = Company.objects.get(pk=seller)
        for j in group_by_order_items_company[seller]:
            listings.append({
                "name": j['product_name'],
                "quantity": j['quantity'],
                "price": j['price'],
                "total": j['total'],
                "image": j['product_image']
            })

        context = CONTEXT_DATA( user = user, 
                                company=company, 
                                listings = listings)
        distance_selling_contract = DISTANCE_SELLING_CONTRACT(context)
        res_data['distance_selling_contracts'].append(distance_selling_contract)
        pre_information_form = PRE_INFORMATION_FORM(context)
        res_data['pre_information_forms'].append(pre_information_form)

    return res_data
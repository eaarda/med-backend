from rest_framework import serializers


class BasketItemSerializer(serializers.Serializer):
    listing = serializers.IntegerField()
    quantity = serializers.CharField(required=False)


class GetTokenSerializer(serializers.Serializer):
    delivery = serializers.CharField()
    items = BasketItemSerializer(many=True)
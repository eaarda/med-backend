from apps.common.models import Document, DocumentKind
from apps.order.models import Order, PaymentStatusType, TransferStatusType, TransferTransaction
from apps.payment.serializers import GetTokenSerializer
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, HttpResponse
from pathlib import Path
import time, base64, hmac, hashlib, requests, json, os, random
from .utils import CREATE_ORDER_FOR_PAYMENT, STOCK_CONTROL, PAYTR_TOKEN_FOR_PAYMENT, CREATE_ORDER_CONTRACTS
from apps.order.utils import BUYER_NEW_ORDER_MAIL, SELLER_NEW_ORDER_MAIL
from apps.common.serializers import DocumentSerializer
from django.conf import settings
from django.core.files import File

class GetTokenView(CreateAPIView):
    serializer_class = GetTokenSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):

            stock_control = STOCK_CONTROL(request.data)
            if stock_control == False:
                return Response({'error': 'Stock Control'}, status=400)

            order_data = CREATE_ORDER_FOR_PAYMENT(self, request.data, request.user)
            if order_data==None:
                return Response({'error': 'Invalid order'}, status=400)
            
            # Create and Sava Contracts
            contracts = CREATE_ORDER_CONTRACTS(order_data)
            file_name = str(order_data['order_no']) + '.json'

            path = os.path.join("{}\documents\contracts".format(settings.MEDIA_ROOT))
            if not os.path.exists(path):
                os.makedirs(path)

            with open(os.path.join(path, 'temp'+file_name), 'w') as outfile:
                json.dump(contracts, outfile, indent=4)
            document = Document.objects.create(name=file_name, kind=DocumentKind.CONTRACT, account_id=order_data['account'])
            document.document.save(file_name, File(open('{}/{}'.format(path, 'temp'+file_name), 'rb')))
            os.remove('{}/{}'.format(path, 'temp'+file_name)) 

            order_obj = Order.objects.get(id=order_data['id'])
            order_obj.document = document
            order_obj.save()

            
           # Get Token
            token = PAYTR_TOKEN_FOR_PAYMENT(request, order_data)
            return Response(token, 200)
        
        return Response(serializer.errors, status=400)


        
@csrf_exempt
def paytr_callback(request):
    if request.method != 'POST':
        return HttpResponse(str(''))

    post = request.POST
    merchant_key = os.environ.get('PAYTR_MERCHANT_KEY').encode('utf-8')
    merchant_salt = os.environ.get('PAYTR_MERCHANT_SALT')

    # iFrame API Ödeme Sonuç Bildirimi
    if 'merchant_oid' in post:
        hash_str = post['merchant_oid'] + merchant_salt + post['status'] + post['total_amount']
        hash = base64.b64encode(hmac.new(merchant_key, hash_str.encode(), hashlib.sha256).digest())
        hash = hash.decode('utf-8')

        if hash != post['hash']:
            return HttpResponse(str('PAYTR notification failed: bad hash'))
    
        merchant_oid = post['merchant_oid']
        order = Order.objects.get(order_no=merchant_oid)

        if post['status'] == 'success':  # Ödeme Onaylandı
            for order_item in order.order_items.all():
                order_item.payment_status = PaymentStatusType.PAID
                order_item.save()
            print(request)
            BUYER_NEW_ORDER_MAIL(order)
            SELLER_NEW_ORDER_MAIL(order)

        else:  # Ödemeye Onay Verilmedi
            for order_item in order.order_items.all():
                order_item.payment_status = PaymentStatusType.UNPAID
                order_item.save()
            print(request)

    # Platform Transfer Talimatının Sonucunun Alınması
    if 'trans_ids' in post:
        trans_ids_mutable = post['trans_ids']
        trans_ids_mutable = trans_ids_mutable.replace('\\', '')
        hash_str = trans_ids_mutable + merchant_salt
        hash = base64.b64encode(hmac.new(merchant_key, hash_str.encode(), hashlib.sha256).digest())
        hash = hash.decode('utf-8')
        
        if str(hash) != post['hash']:
            return HttpResponse(str('PAYTR notification failed: bad hash'))

        trans_ids = json.loads(trans_ids_mutable)
        
        for ids in trans_ids:
        # Örn: Burada trans_id ile veritabanınızdan transfer talebini tespit edip ilgili kullanıcınıza bilgilendirme gönderebilirsiniz (email, sms vb.)
            print(ids)
            try:
                transfer = TransferTransaction.objects.get(trans_id=ids)
                transfer.order_item.transfer_status = TransferStatusType.TRANSFERRED
                transfer.order_item.save()
            except:
                pass

    # Geri Dönen Ödeme Talimatının Sonucunun Alınması
    if 'mode' in post:
        hash_str = post['merchant_id'] + post['trans_id'] + merchant_salt
        hash = base64.b64encode(hmac.new(merchant_key, hash_str.encode(), hashlib.sha256).digest())
        hash = hash.decode('utf-8')

        if hash != post['hash']:
            return HttpResponse(str('PAYTR notification failed: bad hash'))

        processed_result = json.loads(post['processed_result'])

        for trans in processed_result:
            transfer = TransferTransaction.objects.get(trans_id=post['trans_id'])
        # Burada her işlem için gerekli veri tabanı vb. işlemleri yapabilirsiniz.
            if trans['result'] == 'success':
                transfer.order_item.transfer_status == TransferStatusType.TRANSFERRED
                transfer.order_item.save()
            else:
                transfer.order_item.transfer_status == TransferStatusType.TRANSFER_FAILED
                transfer.order_item.save()
        

    return HttpResponse(str('OK'))
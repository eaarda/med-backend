from apps.listing.serializers import ListingSerializer
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer
from apps.order.models import Order, OrderItem, Shipment
from apps.common.serializers import CargoCompanySerializer, DocumentSerializer


class ShipmentSerializer(WritableNestedModelSerializer):
    cargo_company_display = serializers.SerializerMethodField()
    tracking_url = serializers.SerializerMethodField()
    document_url = DocumentSerializer(source='document', read_only=True, required=False)
    class Meta:
        model = Shipment
        fields = (  'id', 'tracking_number', 'tracking_url', 
                    'date', 'cargo_company', 'cargo_company_display', 'note',
                    'document', 'document_url')

    def get_cargo_company_display(self, obj):
        return CargoCompanySerializer(obj.cargo_company).data
    
    def get_tracking_url(self, obj):
        if obj.cargo_company and obj.cargo_company.url:
            return obj.cargo_company.url + obj.tracking_number


class OrderItemSerializer(WritableNestedModelSerializer):
    status_display = serializers.CharField(source='get_status_display', required=False)
    payment_status = serializers.CharField(source='get_payment_status_display', required=False)
    shipment = ShipmentSerializer(required=False)

    class Meta:
        model = OrderItem
        fields = (  'id', 'listing', 'expiry', 
                    'product_name', 'product_image', 'company_id', 'company_name',
                    'price', 'quantity', 'total', 
                    'status', 'status_display', 'payment_status',
                    'commission_rate', 'earning',
                    'cancellation_reason',
                    'shipment' )
        read_only_fields = ('payment_status', )


class OrderSerializer(WritableNestedModelSerializer):
    order_items = OrderItemSerializer(many=True)

    class Meta:
        model = Order
        fields = (  'id', 'account', 'order_no', 'order_date', 'total',
                    'delivery', 'delivery_name', 'delivery_phone', 'delivery_email', 
                    'delivery_address', 'delivery_district', 'delivery_city', 
                    'billing_address', 'billing_district', 'billing_city', 
                    'billing_name', 'tax_office', 'tax_number', 'document',
                    'order_items')

class ContractUserSerializer(serializers.Serializer):
    name = serializers.CharField()
    address = serializers.CharField()
    phone = serializers.CharField()
    email = serializers.CharField()

class ContractItemSerializer(serializers.Serializer):
    name = serializers.CharField()
    quantity = serializers.CharField()
    price = serializers.CharField()
    total = serializers.CharField()
    image = serializers.CharField()


class ContractSerializer(serializers.Serializer):
    company_id = serializers.CharField()
    listings = ContractItemSerializer(many=True)
    user = ContractUserSerializer()
from apps.account.models import Company
from apps.base.views import BaseModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import serializers, status
from rest_framework.generics import CreateAPIView
import json
from pathlib import Path
from django.shortcuts import get_object_or_404
from django.db.models import Q

from apps.order.utils import DISTANCE_SELLING_CONTRACT, CONTEXT_DATA, PRE_INFORMATION_FORM
from apps.order.models import Order, OrderItem, OrderItemStatusType, PaymentStatusType, Shipment, TransferStatusType
from apps.order.serializers import ContractSerializer, OrderSerializer, ShipmentSerializer
from apps.payment.utils import PAYTR_TOKEN_FOR_PAYMENT


class OrderViewSet(BaseModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    http_method_names = ['get', 'post']

    def get_queryset(self):
        if 'pk' in self.kwargs:
            return self.queryset.filter(Q(id=self.kwargs['pk'], account=self.request.user) | Q(id=self.kwargs['pk'], order_items__company_id=self.request.user.company.id )).distinct()
        return self.queryset.filter(id=None)


    def retrieve(self, request, pk=None):
        queryset = self.get_queryset()
        order = get_object_or_404(queryset, pk=pk)

        serializer = self.get_serializer(order).data

        if self.request.user.company == order.account.company:
            return Response(serializer)
        
        total = 0
        delete_items = []
        for item in serializer['order_items']:
            if item['company_id'] != str(request.user.company.id):
                delete_items.append(item['id'])
                continue
            total += float(item['total'])

        serializer['order_items'] = [item for item in serializer['order_items'] if item['id'] not in delete_items]
        serializer['total'] = total
        return Response(serializer)


    def create(self, request):
        data = {"detail": "Method \"POST\" not allowed."}
        return Response(data, status=status.HTTP_405_METHOD_NOT_ALLOWED)


    @action(detail=False, url_path="buyings")
    def buyings(self, request):
        result = self.queryset.filter(account=request.user)

        ordering = request.GET.get('ordering')
        if ordering:
            result = result.order_by(ordering)

        context = {
            'request': self.request
        }
        page = self.paginate_queryset(result)
        if page is not None:
            serializer = self.serializer_class(page, many=True, context=context)
            return self.get_paginated_response(serializer.data)

        serializer = self.serializer_class(result, many=True, context=context)
        return Response(serializer.data)
    

    @action(detail=False, url_path="sellings")
    def sellings(self, request):
        result = Order.objects.filter(Q(order_items__payment_status=PaymentStatusType.PAID) | Q(order_items__payment_status=PaymentStatusType.AWAITING_REPAYMENT) | Q(order_items__payment_status=PaymentStatusType.REPAID)).filter(order_items__company_id=self.request.user.company.id).distinct()

        ordering = request.GET.get('ordering')
        if ordering:
            result = result.order_by(ordering)
            
        context = {
            'request': self.request
        }
        page = self.paginate_queryset(result)
        
        if page is not None:
            serializer = self.serializer_class(page, many=True, context=context)
    
            delete_items = []
            for order in serializer.data:
                total = 0
                for item in order['order_items']:
                    if item['company_id'] != str(request.user.company.id):
                        delete_items.append(item['id'])
                        continue
                    total += float(item['total'])
            
                order['order_items'] = [item for item in order['order_items'] if item['id'] not in delete_items]
                order['total'] = total

            return self.get_paginated_response(serializer.data)

        serializer = self.serializer_class(result, many=True, context=context)
        delete_items = []
        for order in serializer.data:
            for item in order['order_items']:
                if item['listing']['company'] != request.user.company.id:
                    delete_items.append(item['id'])
            
            order['order_items'] = [item for item in order['order_items'] if item['id'] not in delete_items]

            total = 0
            for i in order['order_items']:
                total += float(i['total'])
            order['total'] = total

        return Response(serializer.data)
    

    @action(detail=False, methods=['post'], url_path="set-shipment")
    def set_shipment(self, request):

        for item in request.data:
            try:
                order_item_id = item['id']
                item.pop('id')
                order_item = OrderItem.objects.get(id=order_item_id)

                if order_item.listing.company == request.user.company:
                    serializer = ShipmentSerializer(data=item)
                    if serializer.is_valid(raise_exception=True):
                        shipment = serializer.save()
                        order_item.shipment = shipment
                        order_item.save()
            except :
                pass
            
        return Response(status=status.HTTP_200_OK)
    

    @action(detail=False, methods=['post'], url_path="set-shipment/(?P<shipment_pk>\d+)/update")
    def set_shipment_update(self, request, shipment_pk):

        shipment = get_object_or_404(Shipment, pk=shipment_pk)
        if shipment.order_items.first().company_id != str(request.user.company.id):
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)
        else:
            serializer = ShipmentSerializer(shipment, data=request.data, partial=True)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
    

    @action(detail=False, methods=['post'], url_path="set-status")
    def set_status(self,request):

        for item in request.data:
            try:
                order_item_id = item['id']
                item.pop('id')
                request_status = item['status']
                order_item = OrderItem.objects.get(id=order_item_id)
                if order_item.listing.company == request.user.company:
                    type = 'seller'
                elif order_item.order.account.company == request.user.company:
                    type = 'buyer'

                WORKING_DIR = Path(__file__).resolve().parent
                with open("{}/statuses.json".format(WORKING_DIR), encoding="utf8") as json_file:
                    data = json.load(json_file)

                for i in data['statuses']:
                    if i['value'] == order_item.status and i['type'] == type:
                        if request_status in i['next']:
                            order_item.status = request_status
                            if request_status == OrderItemStatusType.REJECTED or request_status == OrderItemStatusType.CANCELED:
                                order_item.listing.quantity += order_item.quantity
                                order_item.listing.save()
                                order_item.payment_status = PaymentStatusType.AWAITING_REPAYMENT
                                if 'cancellation_reason' in item:
                                    order_item.cancellation_reason = item['cancellation_reason']
                            if request_status == OrderItemStatusType.RECEIVED:
                                order_item.transfer_status = TransferStatusType.AWAITING_TRANSFER
                            order_item.save()
            except :
                pass

        return Response(status=status.HTTP_200_OK)
    
    
    @action(detail=True, methods=['get'], url_path="complete-payment")
    def complete_payment(self, request, pk):
        instance = self.serializer_class(self.get_object()).data

        if instance['account'] != request.user.id:
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)
            
        token = PAYTR_TOKEN_FOR_PAYMENT(request, instance)
        return Response(token, 200)


class ContractView(CreateAPIView):
    serializer_class = ContractSerializer

    def post(self, request):
        res_data = {
            'distance_selling_contracts': [],
            'pre_information_forms': [],
        }

        serializer = self.serializer_class(data=request.data, many=True)
        if serializer.is_valid(raise_exception=True):
            for data in serializer.data:
                company = Company.objects.get(pk=data['company_id'])
                context = CONTEXT_DATA( user = data['user'], 
                                        company=company, 
                                        listings = data['listings'])
                distance_selling_contract = DISTANCE_SELLING_CONTRACT(context)
                res_data['distance_selling_contracts'].append(distance_selling_contract)
                pre_information_form = PRE_INFORMATION_FORM(context)
                res_data['pre_information_forms'].append(pre_information_form)
                
        return Response(res_data, status=status.HTTP_200_OK)
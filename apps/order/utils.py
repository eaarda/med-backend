from http.client import OK
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import get_template, render_to_string
from django.template import Context
from django.core.mail import EmailMessage
import datetime, locale
from django.db.models import Sum
from apps.account.models import Company

from apps.order.serializers import OrderItemSerializer


def BUYER_NEW_ORDER_MAIL(order):
    subject = 'Siparişiniz Alındı' 

    order_items = []
    for i in order.order_items.all():
        order_item = OrderItemSerializer(i).data

        order_items.append({
            'name': order_item['product_name'],
            'quantity': order_item['quantity'],
            'price': order_item['price'],
            'total': order_item['total'],
            'image': 'data&colon;image/jpeg;base64,' + order_item['product_image'],
        })
    
    locale.setlocale(locale.LC_ALL, '')
    order_date = datetime.datetime.strftime(order.order_date, '%d %B %Y, %H:%M')

    context = {
        'order_no': order.order_no,
        'order_date': order_date,
        'total': order.total,
        'order_items': order_items,
        'billing_address': {
            'address': order.billing_address,
            'district': order.billing_district,
            'city': order.billing_city,
            'tax_office': order.tax_office,
            'tax_number': order.tax_number,
        },
        'delivery_address': {
            'address': order.delivery_address,
            'district': order.delivery_district,
            'city': order.delivery_city,
            'phone': order.delivery_phone,
            'email': order.delivery_email,
        },
        'order_url': settings.SITE_HOST + '/orders/',
    }

    html_version = 'buyer_new_order.html'
    html_message = render_to_string(html_version, {'context':context})
    message = EmailMessage(subject, html_message, settings.DEFAULT_FROM_EMAIL, [order.account.email])
    message.content_subtype = 'html'
    return message.send()

def SELLER_NEW_ORDER_MAIL(order):
    subject = 'Yeni Siparişiniz Var'

    order_seller_company = []
    for i in order.order_items.all():
        if i.listing.company not in order_seller_company:
            order_seller_company.append(i.listing.company)

    for seller in order_seller_company:
        order_items = []
        for i in order.order_items.filter(listing__company=seller):
            order_item = OrderItemSerializer(i).data

            order_items.append({
                'name': order_item['product_name'],
                'quantity': order_item['quantity'],
                'price': order_item['price'],
                'total': order_item['total'],
                'image': 'data&colon;image/jpeg;base64,' + order_item['product_image'],
            })
        
        order_items_total = order.order_items.filter(listing__company=seller).aggregate(Sum('total'))['total__sum']
        
        locale.setlocale(locale.LC_ALL, '')
        order_date = datetime.datetime.strftime(order.order_date, '%d %B %Y, %H:%M')

        context = {
            'order_no': order.order_no,
            'order_date': order_date,
            'total': order.total,
            'order_items': order_items,
            'billing_address': {
                'address': order.billing_address,
                'district': order.billing_district,
                'city': order.billing_city,
                'tax_office': order.tax_office,
                'tax_number': order.tax_number,
            },
            'delivery_address': {
                'address': order.delivery_address,
                'district': order.delivery_district,
                'city': order.delivery_city,
                'phone': order.delivery_phone,
                'email': order.delivery_email,
            },
            'order_url': settings.SITE_HOST + '/orders/sales',
            'buyer': order.account.company.short_name,
            'order_items_count' : len(order_items),
            'order_items_total' : order_items_total
        }

        html_version = 'seller_new_order.html'
        html_message = render_to_string(html_version, {'context':context})
        message = EmailMessage(subject, html_message, settings.DEFAULT_FROM_EMAIL, [seller.account.email])
        message.content_subtype = 'html'
        message.send()

    return OK

def CONTEXT_DATA(user, company, listings):
    total = 0
    images = []
    for i in listings:
        total += float(i['price']) * int(i['quantity'])
        images.append(i['image'])
    
    context = {
        'buyer' : user,
        'seller' : {
            'name': company.name,
            'address': company.billing_address + ' ' + company.billing_district.name + '/' + company.billing_city.name,
            'phone': company.company_phone,
            'tax_number': company.tax_number,
            'email': company.account.email
        },
        'listings' : listings,
        'total' : round(total, 2),
        'images': images
    }
    return context


def DISTANCE_SELLING_CONTRACT(context):
    
    html_version = 'distance_selling_contract.html'
    html_message = render_to_string(html_version, {'context':context})

    contract = {
        'seller': context['seller']['name'],
        'images': context['images'],
        'content': html_message
    }

    return contract


def PRE_INFORMATION_FORM(context):
    html_version = 'pre_information_form.html'
    html_message = render_to_string(html_version, {'context':context})

    contract = {
        'seller': context['seller']['name'],
        'images': context['images'],
        'content': html_message
    }

    return contract     
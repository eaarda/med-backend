from django.db import models
from django.utils.translation import gettext_lazy as _
import datetime

from apps.base.models import BaseModel
from apps.account.models import Account, Address
from apps.listing.models import Listing
from apps.common.models import CargoCompany, Document


class PaymentStatusType(models.TextChoices):
    AWAITING_PAYMENT = "awaiting_payment", _("Ödeme Bekleniyor")
    PAID = "paid", _("Ödeme Başarılı")
    UNPAID = "unpaid", _("Ödeme Başarısız")
    AWAITING_REPAYMENT = "awaiting_repayment", _("İade Bekleniyor")
    REPAID = "repaid", _("İade Başarılı")

class OrderItemStatusType(models.TextChoices):
    AWAITING = "awaiting", _("Onay Bekleniyor")
    CONFIRMED = "confirmed", _("Onaylandı")
    REJECTED = "rejected", _("Onaylanmadı")
    CANCELED = "canceled", _("İptal Edildi")
    SHIPPED = "shipped", _("Kargoya Verildi")
    RECEIVED = "received", _("Teslim Alındı")

class TransferStatusType(models.TextChoices):
    AWAITING_RECEIVED = "awaiting_received", _("Teslimat Bekleniyor")
    AWAITING_TRANSFER = "awaiting_transfer", _("Transfer Bekleniyor")
    TRANSFER_STARTED = "transfer_started", _("Transfer Başlatıldı")
    TRANSFERRED = "transferred", _("Transfer Başarılı")
    TRANSFER_FAILED = "transfer_failed", _("Transfer Başarısız")
    

class Shipment(BaseModel):

    tracking_number = models.CharField(max_length=40, blank=True, null=True)
    date = models.DateField(default=datetime.date.today, help_text='Düzenlenme tarihi')
    note = models.TextField(blank=True, null=True)
    cargo_company = models.ForeignKey(CargoCompany, on_delete=models.SET_NULL, related_name='shipments', blank=True, null=True)
    document = models.ForeignKey(Document, on_delete=models.SET_NULL, related_name='shipments', blank=True, null=True)
    class Meta:
        db_table = "shipments"
    
    def __str__(self):
        return self.tracking_number


class Order(BaseModel):
    account = models.ForeignKey(Account, on_delete=models.SET_NULL, related_name='orders', blank=True, null=True)
    order_no = models.CharField(max_length=20, db_index=True)
    order_date = models.DateTimeField(auto_now_add=True)

    delivery = models.ForeignKey(Address, on_delete=models.SET_NULL, blank=True, null=True, related_name='delivery_address')
    delivery_name = models.CharField(max_length=255)
    delivery_phone = models.CharField(max_length=20)
    delivery_email = models.CharField(max_length=255)
    delivery_address = models.CharField(max_length=255)
    delivery_district = models.CharField(max_length=255)
    delivery_city = models.CharField(max_length=255)

    billing_name = models.CharField(max_length=255)
    billing_address = models.CharField(max_length=255)
    billing_district = models.CharField(max_length=255)
    billing_city = models.CharField(max_length=255)
    
    tax_office = models.CharField(max_length=255, blank=True, null=True)
    tax_number = models.CharField(max_length=11, blank=True, null=True)

    total = models.FloatField()
    document = models.ForeignKey(Document, on_delete=models.PROTECT, related_name='orders', blank=True, null=True)

    class Meta:
        db_table = 'orders'
        ordering = ['-created_at']

    def __str__(self):
        return self.account.email


class OrderItem(BaseModel):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_items')
    listing = models.ForeignKey(Listing, on_delete=models.SET_NULL, related_name='order_items', blank=True, null=True)
    expiry = models.DateField(blank=True, null=True)

    product_name = models.CharField(max_length=255, blank=True, null=True)
    product_image = models.TextField(blank=True, null=True)
    company_id = models.CharField(max_length=255, blank=True, null=True)
    company_name = models.CharField(max_length=255, blank=True, null=True)

    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField()
    total = models.DecimalField(max_digits=10, decimal_places=2)

    status = models.CharField(max_length=20, choices=OrderItemStatusType.choices, default=OrderItemStatusType.AWAITING)
    payment_status = models.CharField(max_length=20, choices=PaymentStatusType.choices, default=PaymentStatusType.AWAITING_PAYMENT)
    transfer_status = models.CharField(max_length=20, choices=TransferStatusType.choices, default=TransferStatusType.AWAITING_RECEIVED)
    
    commission_rate = models.DecimalField(max_digits=10, decimal_places=2)
    earning = models.DecimalField(max_digits=10, decimal_places=2)

    shipment = models.ForeignKey(Shipment, on_delete=models.SET_NULL, related_name='order_items', blank=True, null=True)

    cancellation_reason = models.CharField(max_length=255, blank=True, null=True, help_text='Satıcı iptal sebebi')

    class Meta:
        db_table = 'order_items'
        ordering = ['-created_at']

    def __str__(self):
        return '{} - {}'.format(self.order.order_date, self.order.order_no)


class TransferTransaction(BaseModel):
    order_item = models.ForeignKey(OrderItem, on_delete=models.CASCADE, related_name='transfer_transactions')
    trans_id = models.CharField(max_length=60, blank=True, null=True)
    submerchant_amount = models.DecimalField(max_digits=10, decimal_places=2)
    submerchant_iban =  models.CharField(max_length=26, blank=True, null=True)
    submerchant_name = models.CharField(max_length=255, blank=True, null=True)
 
    class Meta:
        db_table = 'transfer_transactions'
        ordering = ['-created_at']

    def __str__(self):
        return self.trans_id
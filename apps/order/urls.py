from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ContractView, OrderViewSet

router = DefaultRouter()
router.register(r'orders', OrderViewSet)


urlpatterns = [
    path('', include(router.urls)),

    path('contracts/', ContractView.as_view(), name='contracts'),
]
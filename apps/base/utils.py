import re, json, unidecode
from django.template.defaultfilters import slugify


def CREATE_SLUG(text):
    decode = unidecode.unidecode(text).lower()
    text = re.sub(r'[\W_]+', '-', decode)
    return slugify(text)
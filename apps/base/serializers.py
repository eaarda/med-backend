from rest_framework import serializers


class BaseSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        read_only_fields = ('id', 'created_at', 'updated_at')
    
    def to_representation(self, instance):

        data = super(BaseSerializer, self).to_representation(instance)

        image_data = {
            "id":None,
            "kind":"other",
            "image":"/images/default-image.jpg",
            "created_at": None
        }

        if (hasattr(instance, 'image') and instance.image==None):
            data['image_url'] = image_data
        
        if (hasattr(instance, 'favicon') and instance.image==None):
            data['favicon_url'] = image_data
        
        return data
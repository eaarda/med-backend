from django.db import models

# Create your models here.
    
    
class BaseModel(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)
    active = models.BooleanField(default=True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
from rest_framework import viewsets
from rest_framework import filters
from url_filter.integrations.drf import DjangoFilterBackend
from admin.base.filters import CaseInsensitiveOrderingFilter


class BaseModelViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend, CaseInsensitiveOrderingFilter]
    filter_fields = '__all__'
    ordering_fields = '__all__'
    list_serializer_class = None

    class Meta:
        abstract = True
    

class BaseReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
    filter_backends = [DjangoFilterBackend, CaseInsensitiveOrderingFilter]
    filter_fields = '__all__'
    ordering_fields = '__all__'
    list_serializer_class = None

    class Meta:
        abstract = True
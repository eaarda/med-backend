#!/bin/bash

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

# Load defaults
# echo "Load defaults"
# python manage.py load_defaults

# Start server
echo "Starting server"
gunicorn backend.wsgi:application --bind 0.0.0.0:8000 --workers=2

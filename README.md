[![Docker Image CI](https://github.com/medikaport/backend/actions/workflows/docker-image.yml/badge.svg)](https://github.com/medikaport/backend/actions/workflows/docker-image.yml)

# medikaport-backend

```
poetry shell
```

```
poetry export -f requirements.txt --output requirements.txt
```

python manage.py create_products
(İlk defa çalıştırıldığında ürünleri resimsiz olarak yükler, daha sonraki çalıştırmalarda eksik resimleri tamamlar)


## .env içeriği

```
# django
DJANGO_DEBUG=
DJANGO_SECRET_KEY=

# database
POSTGRES_HOST=
POSTGRES_PORT=
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_DB=

# email
EMAIL_HOST=
EMAIL_HOST_USER=
EMAIL_HOST_PASSWORD=
DEFAULT_FROM_EMAIL=
SITE_HOST=
ADMIN_HOST = 
NOTIFICATION_EMAIL = 

# paytr
PAYTR_MERCHANT_ID=
PAYTR_MERCHANT_KEY=
PAYTR_MERCHANT_SALT=
PAYTR_TEST_MODE=

# storage
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_STORAGE_BUCKET_NAME=
AWS_S3_ENDPOINT_URL=
AWS_LOCATION=

```
# LOCAL
## Build and push container
```
docker build --tag ghcr.io/medikaport/backend . && docker push ghcr.io/medikaport/backend
```

 # SERVER

## Create network
```
docker network create proxy
```

## Stop and remove old container with docker image
```
docker stop backend && \
docker rm backend && \
docker image rm ghcr.io/medikaport/backend
```

## Get new docker image and run container
```
docker run \
  --expose 8000 \
  --detach \
  --restart always \
  --name backend \
  --env-file ~/env/medikaport.env \
  --network proxy \
  ghcr.io/medikaport/backend
```

## Build and run proxy container with nginx
````
docker build -t proxy . \
&& docker run \
  --publish 80:80 \
  --detach \
  --restart always \
  --name proxy \
  --network proxy \
  proxy
```

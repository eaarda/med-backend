from apps.product.utils import REMOVE_DUPLICATE_DATA
from rest_framework.response import Response
from apps.product.serializers import CategorySerializer, BrandSerializer


def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()


def serialize_data(viewset, data):
    page = viewset.paginate_queryset(data)
    if page is not None:
        serializer = viewset.get_serializer(page, many=True)
        return viewset.get_paginated_response(serializer.data)

    serializer = viewset.get_serializer(data, many=True)
    return Response(serializer.data)   


def serialize_data_with_filters(viewset, data):

    all_categories = []
    all_brands = []
    
    for i in data:
        if i.category:
            all_categories.append(i.category)
        if i.brand:
            all_brands.append(i.brand)

    categories_data = CategorySerializer(all_categories, many=True).data
    categories = REMOVE_DUPLICATE_DATA(categories_data)
    category_count = {}
    for i in categories:
        category_count[i['id']] = 0
        for j in data:
            if j.category:
                if i['id'] == j.category.id:
                    category_count[i['id']] += 1
    for category in categories:
            category['product_count'] = category_count[category['id']]
    

    brands_data = BrandSerializer(all_brands, many=True).data
    brands = REMOVE_DUPLICATE_DATA(brands_data)
    brand_count = {}
    for i in brands:
        brand_count[i['id']] = 0
        for j in data:
            if j.brand:
                if i['id'] == j.brand.id:
                    brand_count[i['id']] += 1
    for brand in brands:
            brand['product_count'] = brand_count[brand['id']]
    
    
    page = viewset.paginate_queryset(data)
    if page is not None:
        serializer = viewset.get_serializer(page, many=True)
        response = viewset.get_paginated_response(data=serializer.data)
        response.data['filters'] = {
            'categories': categories,
            'brands': brands
        }
        return Response(response.data)

    
    serializer = viewset.get_serializer(data, many=True)
    return Response(serializer.data) 
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include

from .views import get_homepage_slider


urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/', include('apps.common.urls')),
    path('api/', include('apps.account.urls')),
    path('api/', include('apps.product.urls')),
    path('api/', include('apps.listing.urls')),
    path('api/', include('apps.cart.urls')),
    path('api/', include('apps.payment.urls')),
    path('api/', include('apps.order.urls')),

    path('api/admin/', include('admin.account.urls')),
    path('api/admin/', include('admin.product.urls')),
    path('api/admin/', include('admin.listing.urls')),
    path('api/admin/', include('admin.order.urls')),
    path('api/admin/', include('admin.common.urls')),
    path('api/admin/', include('admin.payment.urls')),

    path(r'api/sliders/homepage/', get_homepage_slider),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + \
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
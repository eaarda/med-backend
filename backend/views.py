from django.http import JsonResponse

def get_homepage_slider(request):
    """
    Returns main slider items.
    """
    if request.method == 'GET':
        return JsonResponse({
            "statusCode":200,
            "statusMessage":"OK",
            "results":[
                {
                    "redirectUrl":"#",
                    "imageUrl":"https://i.hizliresim.com/fxkmkuo.png",
                    "smallImageUrl":"https://i.hizliresim.com/fxkmkuo.png",
                    "mobileWebImageUrl":"https://i.hizliresim.com/fxkmkuo.png",
                    "queueNo":1,
                    "gtm":"None"
                },
                {
                    "redirectUrl":"http://app.medikaport.com/listing/12",
                    "imageUrl":"https://i.hizliresim.com/d4zl0ys.png",
                    "smallImageUrl":"https://i.hizliresim.com/d4zl0ys.png",
                    "mobileWebImageUrl":"https://i.hizliresim.com/d4zl0ys.png",
                    "queueNo":3,
                    "gtm":"None"
                },
                {
                    "redirectUrl":"#",
                    "imageUrl":"https://i.hizliresim.com/7wxayun.png",
                    "smallImageUrl":"https://i.hizliresim.com/7wxayun.png",
                    "mobileWebImageUrl":"https://i.hizliresim.com/7wxayun.png",
                    "queueNo":4,
                    "gtm":"None"
                },
                
            ],
            "errors":"None"
            }
            , safe=False
        )

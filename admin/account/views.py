from multiprocessing import context
from rest_framework import viewsets
from rest_framework import serializers, status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from django.contrib.auth import authenticate, login
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.views import APIView
from django.db.models import Q
from datetime import datetime

from admin.base.views import AdminBaseModelViewSet
from apps.account.models import Account
from apps.account.utils import CONFIRMED_MAIL
from .serializers import LoginSerializer, AccountSerializer, CreateAccountSerializer, SendCustomerMailSerializer

class LoginViewSet(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.data['email']
        password = serializer.data['password']

        # User Control
        if not Account.objects.filter(email=email).exists():
            raise serializers.ValidationError({'invalid':['Invalid username or password']})
        
        user = authenticate(email=email, password=password)
        if not user or user.is_anonymous or not user.is_admin or not user.is_staff:
            raise serializers.ValidationError({'invalid':['Invalid username or password']})
        
        refresh = RefreshToken.for_user(user)

        data = {
            'refresh':str(refresh),
            'access':str(refresh.access_token),
            'is_admin':user.is_admin,
            'email':user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
        }

        login(request,user)
        return Response(data, status.HTTP_200_OK)


class AdminAuthorizationViewSet(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):

        data = {
            'is_admin':request.user.is_admin,
            'email':request.user.email,
            'first_name':request.user.first_name,
            'last_name':request.user.last_name,
        }

        return Response(data, status.HTTP_200_OK)


class AccountViewSet(AdminBaseModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    search_fields = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'company__name',
        'company__tax_number',
        'company__tax_office',
    ]
    ordering_case_insensitive_fields = [
        'first_name', '-first_name', 
        'last_name', '-last_name', 
        'email', '-email'
    ]

    http_method_names = ['get','patch']

    def perform_update(self, serializer):
        instance = self.get_object()

        if instance.is_active != True and serializer.validated_data.get('is_active') == True:
            CONFIRMED_MAIL(instance)

        if serializer.validated_data.get('is_active') == True and instance.verification_date==None:
            serializer.validated_data['verification_date'] = datetime.now()
        
        serializer.save()


class CreateAccountViewSet(CreateAPIView):
    permission_classes = [IsAdminUser]
    serializer_class = CreateAccountSerializer


from django.template import Context
from django.core.mail import EmailMessage
from django.template.loader import get_template, render_to_string
from django.conf import settings
from django.core.mail import send_mail
import imaplib, time
from django.core.mail import get_connection, send_mail

class SendCustomerMailViewSet(CreateAPIView):
    permission_classes = [IsAdminUser]
    serializer_class = SendCustomerMailSerializer

    def post(self, request):

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        receiver = serializer.data['receiver']
        delegate = serializer.data['delegate']
        subject = serializer.data['subject']
        content =  serializer.data['content']

        context = Context({'delegate': delegate, 'content': content})
        html_version = 'send_customer_email.html'
        html_message = render_to_string(html_version, {'context':context})

        # receiver mail
        with get_connection(host=settings.EMAIL_HOST, port=settings.EMAIL_PORT, username=settings.CUSTOMER_SERVICE_EMAIL, password=settings.CUSTOMER_SERVICE_PASSWORD, use_tls=True) as connection:
            message = EmailMessage(subject, html_message, 'Medikaport <{}>'.format(settings.CUSTOMER_SERVICE_EMAIL), [receiver], connection=connection)
            message.content_subtype = 'html'
            message.send()
        
        text = str(message.message())
        # noreply
        imap = imaplib.IMAP4_SSL('imap.yandex.ru', 993)
        imap.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        imap.append('MusteriHizmetleri', '\\UNSEEN', imaplib.Time2Internaldate(time.time()), text.encode('utf8'))
        imap.logout()
        # customer-service
        imap = imaplib.IMAP4_SSL('imap.yandex.ru', 993)
        imap.login(settings.CUSTOMER_SERVICE_EMAIL, settings.CUSTOMER_SERVICE_PASSWORD)
        imap.append('Sent', '\\Seen', imaplib.Time2Internaldate(time.time()), text.encode('utf8'))
        imap.logout()

        return Response(status.HTTP_200_OK)
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer
from drf_writable_nested.mixins import UniqueFieldsMixin

from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from datetime import datetime
from apps.account.utils import RANDOM_PASSWORD_MAIL

from apps.account.models import Account, Company
from apps.common.models import Setting


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(style={'input_type': 'password'}, max_length=128)
    class Meta:
        model = Account
        fields = ('email', 'password')


class CompanySerializer(WritableNestedModelSerializer, UniqueFieldsMixin):
    billing_city_name = serializers.StringRelatedField(source='billing_city')
    billing_district_name = serializers.StringRelatedField(source='billing_district')
    default_commission_rate = serializers.SerializerMethodField()
    class Meta:
        model = Company
        fields = (  'id', 'name', 'short_name', 'company_no', 'iban', 'bank_account_name', 'commission_rate', 'default_commission_rate',
                    'tax_number', 'tax_office', 'company_phone', 'mms_approval', 'seller_note',
                    'billing_address', 'billing_district', 'billing_city',
                    'billing_city_name', 'billing_district_name'  )
                    
    def get_default_commission_rate(self, obj):
        setting = Setting.objects.first()
        return setting.commission_rate


class AccountSerializer(WritableNestedModelSerializer):
    company = CompanySerializer()
    class Meta:
        model = Account
        fields = (  'id', 'email', 'first_name', 'last_name', 'phone', 
                    'date_joined', 'verification_date',
                    'is_active', 'is_banned', 'is_deleted', 'description',
                    'company')
        read_only_fields = ('verification_date', 'date_joined')


class CreateAccountSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True,validators=[UniqueValidator(queryset=Account.objects.all(), lookup='iexact' )])
    first_name = serializers.CharField(required=True, write_only=True)
    last_name = serializers.CharField(required=True, write_only=True)
    phone = serializers.CharField(required=True, write_only=True, validators=[UniqueValidator(queryset=Account.objects.all(), lookup='iexact' )])

    company = serializers.CharField(required=True, write_only=True)
    short_name = serializers.CharField(required=True, write_only=True, min_length=5, max_length=8, validators=[UniqueValidator(queryset=Company.objects.all(), lookup='iexact')])
    company_no = serializers.CharField(required=True, write_only=True, validators=[UniqueValidator(queryset=Company.objects.all(), lookup='iexact')])
    company_phone = serializers.CharField(required=True, write_only=True, validators=[UniqueValidator(queryset=Company.objects.all(), lookup='iexact')])
    tax_number = serializers.CharField(required=True, write_only=True)
    tax_office = serializers.CharField(required=True, write_only=True)
    billing_address = serializers.CharField(required=True, write_only=True)
    billing_district = serializers.CharField(required=True, write_only=True)
    billing_city = serializers.CharField(required=True, write_only=True)
    mms_approval = serializers.BooleanField(required=False, write_only=True, default=False)

    def create(self, validated_data):
        verification_date= datetime.now()
        user = Account.objects.create(  email=validated_data['email'].lower(),
                                        first_name=validated_data['first_name'],
                                        last_name=validated_data['last_name'],
                                        phone=validated_data['phone'],
                                        verification_date = verification_date,
                                        is_active = False
                                    )
        Company.objects.create( account=user,
                                name=validated_data['company'],
                                short_name=validated_data['short_name'],
                                company_no=validated_data['company_no'],
                                company_phone=validated_data['company_phone'],
                                tax_number=validated_data['tax_number'],
                                tax_office=validated_data['tax_office'],
                                billing_address=validated_data['billing_address'],
                                billing_district_id=validated_data['billing_district'],
                                billing_city_id=validated_data['billing_city'],
                                mms_approval=validated_data['mms_approval'])
        password = Account.objects.make_random_password(length=8, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')
        user.set_password(password)
        user.save()

        # Random Mail
        RANDOM_PASSWORD_MAIL(user, password)

        return validated_data


class SendCustomerMailSerializer(serializers.Serializer):
    receiver = serializers.EmailField(required=True)
    delegate = serializers.CharField(required=True)
    subject = serializers.CharField(required=True)
    content = serializers.CharField(required=True)
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (    LoginViewSet, AccountViewSet, AdminAuthorizationViewSet, CreateAccountViewSet, SendCustomerMailViewSet )

router = DefaultRouter()
router.register(r'accounts', AccountViewSet)

urlpatterns = [
    path('', include(router.urls)),

    path('login/', LoginViewSet.as_view(), name='login'),
    path('create-account/', CreateAccountViewSet.as_view(), name='create-account'),

    path('authorization/', AdminAuthorizationViewSet.as_view(), name='authorization'),

    path('send-customer-mail/', SendCustomerMailViewSet.as_view(), name='send-customer-mail'),
  
]
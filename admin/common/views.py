from admin.base.views import AdminBaseModelViewSet

from rest_framework.response import Response
from rest_framework.views import APIView 

from apps.account.models import Account
from apps.common.models import CargoCompany, Image, Setting, City, Post, ErrorReport
from apps.product.models import Brand, Category, Product, ProductStatus, BrandStatus, CategoryStatus
from apps.listing.models import Listing, ListingStatus
from apps.order.models import Order, OrderItem, PaymentStatusType, TransferStatusType

from admin.common.serializers import ImageSerializer, SettingSerializer, CitySerializer, PostSerializer, CargoCompanySerializer, ErrorReportSerializer
from admin.account.serializers import AccountSerializer
from admin.product.serializers import ProductSerializer, BrandSerializer, CategorySerializer
from admin.order.serializers import OrderItemSerializer


class ImageViewSet(AdminBaseModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer

    def create(self, request, *args, **kwargs):
        data = {
            "results":[],
            "errors":[]
        }

        images = self.request.FILES.getlist('image')
        for image in images:
            try:
                request.data['image'] = image
                request.data['name'] = image.name
                request.data['account'] = self.request.user.id
                serializer = self.serializer_class(data=request.data)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                data['results'].append(serializer.data)
            except:
                data['errors'].append(image.name)

        return Response(data, 200)


class SettingViewSet(AdminBaseModelViewSet):
    queryset = Setting.objects.all()
    serializer_class = SettingSerializer
    http_method_names = ['get', 'put', 'patch']

    def list(self, request):
        serializer = self.serializer_class(self.queryset.filter().first())
        return Response(serializer.data)


class CityViewSet(AdminBaseModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer


class PostViewSet(AdminBaseModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CargoCompanyViewSet(AdminBaseModelViewSet):
    queryset = CargoCompany.objects.all()
    serializer_class = CargoCompanySerializer


class ErrorReportViewSet(AdminBaseModelViewSet):
    queryset = ErrorReport.objects.all()
    serializer_class = ErrorReportSerializer


class StatsView(APIView):

    def get(self, request):
        data = {
            'widgets':[
                {
                    'name':'Ürünler',
                    'active': Product.objects.filter(active=True).count(),
                    'passive': Product.objects.filter(active=False).count(),
                    'total': Product.objects.all().count(),
                },
                {
                    'name':'Kullanıcılar',
                    'active': Account.objects.filter(is_active=True).count(),
                    'passive': Account.objects.filter(is_active=False).count(),
                    'total': Account.objects.all().count(),
                },
                {
                    'name': 'Markalar',
                    'active': Brand.objects.filter(active=True).count(),
                    'passive': Brand.objects.filter(active=False).count(),
                    'total': Brand.objects.all().count(),
                },
                {
                    'name': 'Kategoriler',
                    'active': Category.objects.filter(active=True).count(),
                    'passive': Category.objects.filter(active=False).count(),
                    'total': Category.objects.all().count(),
                },
                {
                    'name': 'İlanlar',
                    'active': Listing.objects.filter(status=ListingStatus.ONLINE).count(),
                    'passive': Listing.objects.filter(status=ListingStatus.OFFLINE).count(),
                    'total': Listing.objects.all().count(),
                },
                {
                    'name': 'Siparişler',
                    'total': Order.objects.all().count(),
                }
            ],
            'waitings':[
                {
                    "name": "Onay Bekleyen Kullanıcılar",
                    "columns": [
                        {
                            "title": "Firma",
                            "type": "company"
                        },
                        {
                            "title": "Durum",
                            "field": "is_active",
                            "type":"account-status"
                        },
                        {
                            "title":"",
                            "type":"actions",
                            "actions": [
                                {
                                    "icon": "fad fa-edit",
                                    "type": "link",
                                    "action":"/accounts/edit/{id}",
                                }
                            ]
                        }
                    ],
                    "data":AccountSerializer(Account.objects.filter(is_active=False), many=True).data
                },
                {
                    "name": "Onay Bekleyen Ürünler",
                    "columns": [
                        {
                            "title": "Adı",
                            "field": "name",
                            "type":"extra"
                        },
                        {
                            "title": "Durum",
                            "field": "status_display",
                            "type": "product-status"
                        },
                        {
                            "title":"",
                            "type":"actions",
                            "actions":[
                                {
                                    "icon": "fad fa-edit",
                                    "type": "link",
                                    "action": "/products/edit/{id}"
                                }
                            ]
                        }
                    ],
                    "data": ProductSerializer(Product.objects.filter(status=ProductStatus.WAITING_APPROVAL), many=True).data
                },
                {
                    "name": "Onay Bekleyen Markalar",
                    "columns": [
                        {
                            "title": "Adı",
                            "field": "name",
                            "type":"extra"
                        },
                        {
                            "title": "Durum",
                            "field": "status",
                            "type": "brand-status"
                        },
                        {
                            "title":"",
                            "type":"actions",
                            "actions":[
                                {
                                    "icon": "fad fa-edit",
                                    "type": "link",
                                    "action": "/brands/edit/{id}"
                                }
                            ]
                        }
                    ],
                    "data": BrandSerializer(Brand.objects.filter(status=BrandStatus.WAITING_APPROVAL), many=True).data
                },
                {
                    "name": "Onay Bekleyen Kategoriler",
                    "columns": [
                        {
                            "title": "Adı",
                            "field": "name",
                            "type":"extra"
                        },
                        {
                            "title": "Durum",
                            "field": "status",
                            "type": "category-status"
                        },
                        {
                            "title":"",
                            "type":"actions",
                            "actions":[
                                {
                                    "icon": "fad fa-edit",
                                    "type": "link",
                                    "action": "/categories/edit/{id}"
                                }
                            ]
                        }
                    ],
                    "data": CategorySerializer(Category.objects.filter(status=CategoryStatus.WAITING_APPROVAL), many=True).data
                },
                {
                    "name": "İnceleme Bekleyen Hata Bildirileri",
                    "columns": [
                        {
                            "title": "İlan",
                            "field": "listing_display.product_display.name"
                        },
                        {
                            "title": "Durum",
                            "field":"active",
                            "type":"is-active"
                        },
                        {
                            "title":"",
                            "type":"actions",
                            "actions":[
                                {
                                    "icon": "fad fa-eye",
                                    "type": "link",
                                    "action": "/reports/view/{id}"
                                }
                            ]
                        }
                    ],
                    "data":ErrorReportSerializer(ErrorReport.objects.filter(active=True), many=True).data
                },
                {
                    "name": "Para İadesi Bekleyen Siparişler",
                    "columns": [
                        {
                            "title":"Sipariş No",
                            "field":"order_no",
                        },
                        {
                            "title":"Alıcı",
                            "field":"buyer"
                        },
                        {
                            "title":"",
                            "type":"actions",
                            "actions":[
                                {
                                    "icon":"fad fa-eye",
                                    "type": "link",
                                    "action": "/orders/view/{order_id}"
                                }
                            ]
                        }
                    ],
                    "data": OrderItemSerializer(OrderItem.objects.filter(payment_status = PaymentStatusType.AWAITING_REPAYMENT), many=True).data
                },
                {
                    "name": "Transfer Bekleyen Siparişler",
                    "columns": [
                        {
                            "title":"Sipariş No",
                            "field":"order_no",
                        },
                        {
                            "title":"Satıcı",
                            "field":"company_name"
                        },
                        {
                            "title":"",
                            "type":"actions",
                            "actions":[
                                {
                                    "icon":"fad fa-eye",
                                    "type": "link",
                                    "action": "/orders/view/{order_id}"
                                }
                            ]
                        }
                    ],
                    "data": OrderItemSerializer(OrderItem.objects.filter(transfer_status=TransferStatusType.AWAITING_TRANSFER), many=True).data
                }
            ]
        }
        
        return Response(data, 200)
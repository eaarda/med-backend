from admin.base.serializers import BaseSerializer
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework.validators import UniqueValidator

from apps.common.models import Image, Setting, SocialMedia, City, District, Post, CargoCompany, ErrorReport
from admin.account.serializers import AccountSerializer


class ImageSerializer(BaseSerializer):

    class Meta:
        model = Image
        fields = ('id', 'kind', 'image', 'created_at')
    
    
    def to_representation(self, instance):

        data = super(ImageSerializer, self).to_representation(instance)
        return {
            "id" : data['id'],
            "kind" : data['kind'],
            "image": instance.image.url,
            "created_at" : data['created_at']
        }


class ImageLiteSerializer(BaseSerializer):
    
    class Meta:
        model = Image
        fields = ['id']


class SocialMediaSerializer(BaseSerializer, WritableNestedModelSerializer):
    
    class Meta:
        model = SocialMedia
        fields = ('id', 'icon', 'url')


class SettingSerializer(BaseSerializer, WritableNestedModelSerializer):
    social_medias = SocialMediaSerializer(many=True, required=False, allow_null=True)
    image_url = ImageSerializer(source='image', read_only=True, required=False)
    favicon_url = ImageSerializer(source='favicon', read_only=True, required=False)

    class Meta:
        model = Setting
        fields = (  'id', 'title', 'motto', 'seperator', 
                    'social_medias', 'phone', 'email',
                    'image', 'image_url', 'favicon', 'favicon_url',
                    'commission_rate', 'maintenance', 'maintenance_message')


class DistrictSerializer(BaseSerializer, WritableNestedModelSerializer):
    
    class Meta:
        model = District
        fields = ('id', 'name')


class CitySerializer(BaseSerializer, WritableNestedModelSerializer):
    district = serializers.SerializerMethodField()
    class Meta:
        model = City
        fields = ('id',  'name', 'district')
    
    def get_district(self, obj):
        if obj.districts:
            return DistrictSerializer(obj.districts, many=True).data
        else:
            return []


class PostSerializer(BaseSerializer):
    title = serializers.CharField(max_length=255, validators=[UniqueValidator(queryset=Post.objects.all(), lookup='iexact' )])
    image_url = ImageSerializer(source='image', read_only=True, required=False)

    class Meta:
        model = Post
        fields = '__all__'


class CargoCompanySerializer(BaseSerializer):
    image_url = ImageSerializer(source='image', read_only=True, required=False)

    class Meta:
        model = CargoCompany
        fields = ('id', 'active', 'name', 'url', 'image', 'image_url')


class ErrorReportSerializer(BaseSerializer):
    
    account_display = serializers.SerializerMethodField()
    listing_display = serializers.SerializerMethodField()
    class Meta:
        model = ErrorReport
        fields = (  'id', 'active', 'created_at', 'updated_at', 
                    'account', 'account_display', 'listing', 'listing_display', 'description')
        read_only_fields = ('account', 'listing', 'description')
    
    def get_account_display(self, obj):
        return AccountSerializer(obj.account).data
    
    def get_listing_display(self, obj):
        from admin.listing.serializers import ListingSerializer
        return ListingSerializer(obj.listing).data
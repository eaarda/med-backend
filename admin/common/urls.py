from django.urls import path, include
from rest_framework import routers
from django.conf.urls import url

from .views import ImageViewSet, SettingViewSet, CityViewSet, StatsView, PostViewSet, CargoCompanyViewSet, ErrorReportViewSet

router = routers.DefaultRouter()

router.register(r"images", ImageViewSet)
router.register(r"settings", SettingViewSet)
router.register(r"cities", CityViewSet)
router.register(r"posts", PostViewSet)
router.register(r"cargo-companies", CargoCompanyViewSet)
router.register(r"error-reports", ErrorReportViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('stats/', StatsView.as_view(), name='stats'),

]
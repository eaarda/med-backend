from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
from django.db.models import Q
from zipfile import ZipFile
import os, io
from PIL import Image as ImageModule
from django.core.files import File

from admin.base.views import AdminBaseModelViewSet

from apps.product.models import Product, Category, Brand, ProductStatus, CategoryStatus, BrandStatus, ProductOwner
from apps.listing.models import Listing, ListingStatus
from apps.account.models import Company
from apps.common.models import Image, MediaKind

from admin.product.serializers import ProductSerializer, CategorySerializer, BrandSerializer

from .utils import GENERATE_NEW_BARCODE


class BrandViewSet(AdminBaseModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    search_fields = [
        'name',
    ]
    ordering_case_insensitive_fields = [
        'name', '-name'
    ]

    
class CategoryViewSet(AdminBaseModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    search_fields = [
        'name',
    ]
    ordering_case_insensitive_fields = [
        'name', '-name'
    ]


class ProductViewSet(AdminBaseModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    search_fields = [
        'name',
        'barcode',
        'model',
        'hic_code',
        'pts_code'
    ]
    ordering_case_insensitive_fields = [
        'name', '-name'
    ]
    
    def perform_update(self, serializer):
        instance = serializer.save()

        if instance.status == ProductStatus.APPROVED:
            for i in instance.listings.filter(status=ListingStatus.WAITING):
                i.status = ListingStatus.ONLINE
                i.save()
    
    # excel = self.request.FILES['excel']
    @action(detail=False, methods=['post'], url_path="upload")
    def upload(self, request):

        # Load the workbook
        wb = load_workbook(filename=request.FILES['data'].file)
        ws = wb.active

        # Read zip file for images data 
        if 'images' in request.FILES:
            zip = request.FILES['images'].file
            zippedImgs = ZipFile(zip)
            images_data = []
            for img_name in zippedImgs.namelist():
                images_data.append({
                    'name': os.path.splitext(os.path.os.path.basename(img_name))[0],
                    'file_name': os.path.basename(img_name),
                    'image': img_name,
                })
    
        # WorkSheet to json format
        last_column = len(list(ws.columns))
        last_row = len(list(ws.rows))
        data=[]
        for row in range(1, last_row + 1):
            obj = {}
            for column in range(1, last_column + 1):
                column_letter = get_column_letter(column)
                if row > 1:
                    obj[ws[column_letter + str(1)].value] = ws[column_letter + str(row)].value
            data.append(obj)


        # Data to model
        for i in data[1:]:
            if i['Ürün Adı']:
                # Barcode Check
                if type(i['Barkod veya UTS']) == float:
                    i['Barkod veya UTS'] = int(i['Barkod veya UTS'])
                barcode = str(i['Barkod veya UTS']) if i['Barkod veya UTS'] else GENERATE_NEW_BARCODE()

                # Category Check
                if i['Kategori'] != None:
                    try:
                        category = Category.objects.get(name=i['Kategori'])
                    except:
                        category = Category.objects.create(name=i['Kategori'], status=CategoryStatus.APPROVED)
                else:
                    category = None
                        
                # Brand Check
                if i['Marka'] != None:
                    try:
                        brand = Brand.objects.get(name=i['Marka'])
                    except:
                        brand = Brand.objects.create(name=i['Marka'], status=BrandStatus.APPROVED)
                else:
                    brand = None

                # Company Check
                if type(i['Firma']) == float:
                    i['Firma'] = int(i['Firma'])
                i['Firma'] = str(i['Firma'])

                try:
                    company = Company.objects.get(company_no=i['Firma'])
                except:
                    company=None

                # Product Check
                product = Product.objects.filter(Q(barcode=barcode) | Q(pts_code=barcode))
                if len(product) == 0:
                    product = Product.objects.create(   name=i['Ürün Adı'], 
                                                        barcode=barcode, 
                                                        pts_code=barcode,
                                                        category=category,
                                                        brand=brand,
                                                        model=i['Model'],
                                                        status=ProductStatus.APPROVED)
                    
                    # Find Product Image
                    if images_data:
                        for img in images_data:
                            if img['name'] == i['Görsel']:
                                data = zippedImgs.read(img['image'])
                                dataEnc = io.BytesIO(data)
                                image_file = ImageModule.open(dataEnc)
                                image = Image.objects.create(kind=MediaKind.PRODUCT, name=img['file_name'], account=company.account)
                                image.image.save(img['file_name'], File(dataEnc))
                            
                                product.image = image
                                product.save()
                    
                    ProductOwner.objects.create(product=product, account=company.account)

                else:
                    product = product[0]
            
                # Listing Check
                if company:
                    quantity = int(i['Stok']) if i['Stok'] else 0
                    try:
                        listing = Listing.objects.get(product=product, company=company)
                        listing.price = i['Fiyat']
                        listing.quantity = quantity
                        listing.save()
                    except:
                        
                        Listing.objects.create( product=product, 
                                                company=company, 
                                                price=i['Fiyat'],
                                                quantity=quantity,
                                                status=ListingStatus.OFFLINE)

        return Response(200)
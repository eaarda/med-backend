from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.db.models import Min
from drf_writable_nested.serializers import WritableNestedModelSerializer

from apps.base.serializers import BaseSerializer
from apps.common.serializers import ImageSerializer, ImageLiteSerializer
from apps.product.models import Product, ProductOwner, Category, Brand


class BrandSerializer(BaseSerializer):
    image_url = ImageSerializer(source='image', read_only=True, required=False)
    product_count = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        fields = '__all__'
    
    def get_product_count(self, obj):
        return obj.products.count()

        
class CategorySerializer(BaseSerializer):
    children = serializers.SerializerMethodField()
    parent_name = serializers.StringRelatedField(source='parent')
    image_url = ImageSerializer(source='image', read_only=True, required=False)
    product_count = serializers.SerializerMethodField()

    def get_children(self, obj):
        if obj.children:
            return CategorySerializer(obj.children, many=True).data
        else:
            return []
    
    def get_product_count(self, obj):
        return obj.products.count()
    
    class Meta:
        model = Category
        fields = '__all__'

        
class ProductSerializer(BaseSerializer, WritableNestedModelSerializer):
    status_display = serializers.CharField(source='get_status_display', required=False)
    barcode = serializers.CharField(required=True, validators=[UniqueValidator(queryset=Product.objects.all(), lookup='iexact')])
    seller_count = serializers.SerializerMethodField()
    minimum_price = serializers.SerializerMethodField()

    image_url = ImageSerializer(source='image', read_only=True, required=False)
    images = ImageLiteSerializer(many=True, allow_null=True, required=False)
    images_url = ImageSerializer(source='images', many=True, read_only=True, required=False)

    category_display = serializers.SerializerMethodField()
    brand_display = serializers.SerializerMethodField()
    account = serializers.SerializerMethodField()

    def get_seller_count(self, obj):
        return obj.listings.count()

    def get_minimum_price(self, obj):    
        return obj.listings.aggregate(min_price=Min('price'))['min_price']
    
    def get_category_display(self, obj):
        return CategorySerializer(obj.category).data
    
    def get_brand_display(self, obj):
        return BrandSerializer(obj.brand).data
    
    def get_account(self, obj):
        try:
            owner = ProductOwner.objects.get(product=obj)
            return {
                "id": owner.account.id,
                "name": owner.account.email,
                "phone": owner.account.phone,
                "company": owner.account.company.name,
                "company_no": owner.account.company.company_no,
            }
        except:
            return None

    class Meta:
        model = Product
        fields = (  'id', 'active', 'account', 'status', 'status_display', 'name', 'barcode',
                    'image', 'image_url', 'images', 'images_url',
                    'price', 'seller_count', 'minimum_price', 
                    'brand', 'brand_display', 'category', 'category_display',
                    'hic_code', 'parent_name', 'model', 'pts_code',
                    'description', 'is_featured')
from datetime import datetime

from apps.product.models import Product


def GENERATE_NEW_BARCODE():
        date = str(datetime.now().strftime("%m%d%H%m%S"))
        count = Product.objects.count()
        return "MP-%s%s" % (date, count+1)
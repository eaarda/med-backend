from rest_framework import viewsets
from admin.base.views import AdminBaseModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action

from apps.order.models import Order, OrderItem, PaymentStatusType, TransferStatusType
from admin.order.serializers import OrderSerializer
from .utils import REPAID, PAYMENT_TRANSFER


class OrderViewSet(AdminBaseModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    search_fields = [
        'order_no',
        'account__company__name'
    ]

    @action(detail=False, methods=['post'], url_path="repaid")
    def repaid(self, request):
        data = {
            "success": [],
            "errors": []
        }

        for item in request.data:
            try:
                order_item_id = item['id']
                order_item = OrderItem.objects.get(id=order_item_id)
                if order_item.payment_status == PaymentStatusType.AWAITING_REPAYMENT:
                    repaid = REPAID(order_item)
                    if repaid == True:
                        data['success'].append(order_item_id)
                    else:
                        data['errors'].append(order_item_id)
                else:
                    data['errors'].append(order_item_id)
            except:
                data['errors'].append(order_item_id)

        return Response(data, 200)
    
    @action(detail=False, methods=['post'], url_path="transfer")
    def transfer(self, request):
        data = {
            "success": [],
            "errors": []
        }

        for item in request.data:
            try:
                order_item_id = item['id']
                order_item = OrderItem.objects.get(id=order_item_id)
                if order_item.transfer_status == TransferStatusType.AWAITING_TRANSFER:
                    transfer = PAYMENT_TRANSFER(order_item)
                    if transfer == True:
                        data['success'].append(order_item_id)
                    else:
                        data['errors'].append(order_item_id)
                else:
                    data['errors'].append(order_item_id)
            except:
                data['errors'].append(order_item_id)

        return Response(data, 200)
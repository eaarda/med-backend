from rest_framework.response import Response
import base64, hmac, hashlib, requests, json, os, random
from pathlib import Path


from apps.order.models import PaymentStatusType, TransferStatusType, TransferTransaction


def REPAID(order_item):

    merchant_id = os.environ.get('PAYTR_MERCHANT_ID')
    merchant_key = os.environ.get('PAYTR_MERCHANT_KEY').encode('utf-8')
    merchant_salt = os.environ.get('PAYTR_MERCHANT_SALT')
    merchant_oid = str(order_item.order.order_no)
    return_amount = str(order_item.total)

    hash_str = merchant_id + merchant_oid + return_amount + merchant_salt
    paytr_token = base64.b64encode(hmac.new(merchant_key, hash_str.encode(), hashlib.sha256).digest())

    params = {
        'merchant_id': merchant_id,
        'merchant_oid': merchant_oid,
        'return_amount': return_amount,
        'paytr_token': paytr_token
    }

    result = requests.post('https://www.paytr.com/odeme/iade', params)
    res = json.loads(result.text)

    if res['status'] == 'success':
        order_item.payment_status = PaymentStatusType.REPAID
        order_item.save()
        print(res)
        return True

    print(res)
    return False


def PAYMENT_TRANSFER(order_item):
    
    merchant_id = os.environ.get('PAYTR_MERCHANT_ID')
    merchant_key = os.environ.get('PAYTR_MERCHANT_KEY').encode('utf-8')
    merchant_salt = os.environ.get('PAYTR_MERCHANT_SALT')
    
    merchant_oid = str(order_item.order.order_no)
    total_amount = int(order_item.order.total * 100)
    submerchant_amount = int(order_item.earning * 100)

    trans_id = order_item.order.order_no + 'TT' + str(order_item.id)
    trans_info = TransferTransaction.objects.create(    order_item=order_item, 
                                                        trans_id=trans_id, 
                                                        submerchant_amount=submerchant_amount,
                                                        submerchant_iban=order_item.listing.company.iban,
                                                        submerchant_name=order_item.listing.company.bank_account_name )

    transfer_name = trans_info.submerchant_name
    transfer_iban = trans_info.submerchant_iban

    hash_str = merchant_id + merchant_oid + trans_id + str(submerchant_amount) + str(total_amount) + transfer_name + transfer_iban + merchant_salt
    paytr_token = base64.b64encode(hmac.new(merchant_key, hash_str.encode(), hashlib.sha256).digest())

    params = {
        'merchant_id': merchant_id,
        'merchant_oid': merchant_oid,
        'trans_id': trans_id,
        'submerchant_amount': submerchant_amount,
        'total_amount': total_amount,
        'transfer_name': transfer_name,
        'transfer_iban': transfer_iban,
        'paytr_token': paytr_token
    }

    result = requests.post('https://www.paytr.com/odeme/platform/transfer', params)
    res = json.loads(result.text)

    if res['status'] == 'success':
        order_item.transfer_status = TransferStatusType.TRANSFER_STARTED
        order_item.save()
        print(res)
        return True
    
    print(res['err_no'] + ' - ' + res['err_msg'])
    text = res['err_no'] + ' - ' + res['err_msg']

    WORKING_DIR = Path(__file__).resolve().parent
    with open('{}/errors.txt'.format(WORKING_DIR), 'a', encoding='utf-8') as f:
        f.write(text)
        f.write("\n")
        f.close()
    
    return False
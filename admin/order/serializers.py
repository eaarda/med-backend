from apps.order.serializers import ShipmentSerializer
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer

from apps.order.models import Order, OrderItem
from admin.listing.serializers import ListingSerializer
from admin.account.serializers import AccountSerializer


class OrderItemSerializer(WritableNestedModelSerializer):
    listing = ListingSerializer()
    status = serializers.CharField(source='get_status_display', required=False)
    payment_status = serializers.CharField(source='get_payment_status_display', required=False)
    transfer_status = serializers.CharField(source='get_transfer_status_display', required=False)
    shipment = ShipmentSerializer(required=False)
    order_no = serializers.CharField(source='order.order_no', required=False)
    order_id = serializers.IntegerField(source='order.id', required=False)
    buyer = serializers.CharField(source='order.account.company.short_name')

    class Meta:
        model = OrderItem
        fields = (  'id', 'order_id', 'order_no', 'buyer', 'listing', 'expiry', 'product_name', 'product_image', 'company_id', 'company_name',
                    'price', 'quantity', 'total', 'status', 'payment_status', 'transfer_status',
                    'commission_rate', 'earning',
                    'cancellation_reason', 'shipment')
        read_only_fields = ('__all__', )


class OrderSerializer(WritableNestedModelSerializer):
    order_items = OrderItemSerializer(many=True)
    account_display = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = (  'id', 'account', 'account_display', 'order_no', 'order_date', 'total',
                    'delivery', 'delivery_name', 'delivery_phone', 'delivery_email', 
                    'delivery_address', 'delivery_district', 'delivery_city', 
                    'billing_address', 'billing_district', 'billing_city', 
                    'billing_name', 'tax_office', 'tax_number', 'document',
                    'order_items')
        read_only_fields = (    'order_no', 'order_date', 'total', 'account', 'account_display',
                                'billing_address', 'billing_district', 'billing_city',
                                'billing_name', 'tax_office', 'tax_number', 'document',)
    
    def get_account_display(self, obj):
        return AccountSerializer(obj.account).data
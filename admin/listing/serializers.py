from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer

from admin.base.serializers import BaseSerializer
from admin.product.serializers import ProductSerializer
from apps.listing.models import Listing
from admin.common.serializers import ImageLiteSerializer, ImageSerializer


class ListingSerializer(BaseSerializer, WritableNestedModelSerializer):
    company_display = serializers.SerializerMethodField()
    product_display = serializers.SerializerMethodField()

    image_url = ImageSerializer(source='image', read_only=True, required=False)
    images = ImageLiteSerializer(many=True, allow_null=True, required=False)
    images_url = ImageSerializer(source='images', many=True, read_only=True, required=False)

    class Meta:
        model = Listing
        fields = (  'id', 'created_at', 'product', 'product_display', 'product_code', 'company', 'company_display',
                    'status', 'expiry', 'price', 'buying_price', 'shipment_kind',
                    'quantity', 'description', 'max_quantity', 'min_quantity',
                    'is_secondhand', 'condition', 'usage_time', 'image', 'image_url', 'images', 'images_url')
    
    def get_company_display(self, obj):
        return {
            "id": obj.company.id,
            "name":obj.company.name,
        }
    
    def get_product_display(self, obj):
        return ProductSerializer(obj.product).data
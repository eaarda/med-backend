from rest_framework import viewsets
from admin.base.views import AdminBaseModelViewSet
from django.db.models import Q

from apps.listing.models import Listing
from admin.listing.serializers import ListingSerializer


class ListingViewSet(AdminBaseModelViewSet):
    queryset = Listing.objects.all()
    serializer_class = ListingSerializer
    search_fields = [
        'product__name', 
        'company__name'
    ]
    ordering_case_insensitive_fields = [
        'product__name', '-product__name', 
        'company__name', '-company__name'
    ]
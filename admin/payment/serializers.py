from rest_framework import serializers


class ReturnedTransfersSerializer(serializers.Serializer):
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()


class SendReturnedTransferSerializer(serializers.Serializer):
    trans_id = serializers.CharField()
    receiver = serializers.CharField()
    iban = serializers.CharField()

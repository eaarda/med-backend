from apps.order.models import TransferTransaction
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
import os, base64, hmac, hashlib, json, requests

from .serializers import ReturnedTransfersSerializer, SendReturnedTransferSerializer


class ReturnedTransfers(CreateAPIView):
    serializer_class = ReturnedTransfersSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            merchant_id = os.environ.get('PAYTR_MERCHANT_ID')
            merchant_key = os.environ.get('PAYTR_MERCHANT_KEY').encode('utf-8')
            merchant_salt = os.environ.get('PAYTR_MERCHANT_SALT')

            start_date = serializer.data['start_date'].replace('T', ' ').replace('Z', '')
            end_date = serializer.data['end_date'].replace('T', ' ').replace('Z', '')

            # Bu kısımda herhangi bir değişiklik yapmanıza gerek yoktur.
            hash_str = merchant_id + start_date + end_date + merchant_salt
            paytr_token = base64.b64encode(hmac.new(merchant_key, hash_str.encode(), hashlib.sha256).digest())

            params = {
                'merchant_id': merchant_id,
                'start_date': start_date,
                'end_date': end_date,
                'paytr_token': paytr_token
            }
            
            result = requests.post('https://www.paytr.com/odeme/geri-donen-transfer', params)
            res = json.loads(result.text)

            if res['status'] == 'success':
                # VT işlemleri vs.
                print(res)
            elif res['status'] == 'failed':
                print('İlgili tarih araliginda islem bulunamadi')
            else:
                print(res['err_no'] + ' - ' + res['err_msg'])

        return Response(res, 200)

class SendReturnedTransfer(CreateAPIView):
    serializer_class = SendReturnedTransferSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            merchant_id = os.environ.get('PAYTR_MERCHANT_ID')
            merchant_key = os.environ.get('PAYTR_MERCHANT_KEY').encode('utf-8')
            merchant_salt = os.environ.get('PAYTR_MERCHANT_SALT')

            try:
                transfer = TransferTransaction.objects.get(trans_id=serializer.data['trans_id'])
            except TransferTransaction.DoesNotExist:
                return Response({'message': 'trans_id does not exist'}, 400)

            trans_info = [
                {
                    'amount': str(transfer.submerchant_amount),
                    'receiver': serializer.data['receiver'],
                    'iban': serializer.data['iban']
                }
            ]

            # Bu kısımda herhangi bir değişiklik yapmanıza gerek yoktur.
            hash_str = merchant_id + transfer.trans_id + merchant_salt
            paytr_token = base64.b64encode(hmac.new(merchant_key, hash_str.encode(), hashlib.sha256).digest())

            params = {
                'trans_info': json.dumps(trans_info),
                'trans_id': transfer.trans_id,
                'merchant_id': merchant_id,
                'paytr_token': paytr_token
            }

            result = requests.post('https://www.paytr.com/odeme/hesaptan-gonder', params)
            res = json.loads(result.text)


            if res['status'] == 'success':
                # status ve trans_id içerir
                print(res)
                transfer.submerchant_iban = trans_info[0]['receiver']
                transfer.submerchant_name = trans_info[0]['iban']
                transfer.save()
            else:
                # status = error
                # status ve err_no - err_msg içerir
                print(res['err_no'] + ' - ' + res['err_msg'])

        return Response(res, 200)
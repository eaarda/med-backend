from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ReturnedTransfers, SendReturnedTransfer


router = DefaultRouter()


urlpatterns = [

    path('returned-transfers/', ReturnedTransfers.as_view(), name='returned-transfers'),
    path('send-returned-transfer/', SendReturnedTransfer.as_view(), name='send-returned-transfer'),
    
    path('', include(router.urls)),
]
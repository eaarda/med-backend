from rest_framework import viewsets
from url_filter.integrations.drf import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAdminUser
from .filters import CaseInsensitiveOrderingFilter


class AdminBaseModelViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend, SearchFilter, CaseInsensitiveOrderingFilter]
    filter_fields = '__all__'
    ordering_fields = '__all__'
    list_serializer_class = None
    permission_classes = [IsAdminUser]

    class Meta:
        abstract = True
    

class AdminBaseReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
    filter_backends = [DjangoFilterBackend, SearchFilter, CaseInsensitiveOrderingFilter]
    filter_fields = '__all__'
    ordering_fields = '__all__'
    list_serializer_class = None
    permission_classes = [IsAdminUser]

    class Meta:
        abstract = True
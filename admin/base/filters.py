from django.db.models.functions import Lower
from rest_framework.filters import OrderingFilter

class CaseInsensitiveOrderingFilter(OrderingFilter):
    
    allowed_custom_filters = [  
        'name',  'created_at', 'updated_at', 
        'email', 'date_joined',
        'product__name', 'price', 'company__name', 'quantity',
        'order_no', 'order_date', 'total',
        'quantity', 'payment_status',
        'is_secondhand'
    ]

    def get_ordering(self, request, queryset, view):
        params = request.query_params.get(self.ordering_param)
        if params:
            fields = [param.strip() for param in params.split(',')]
            ordering = [f for f in fields if f.lstrip('-') in self.allowed_custom_filters]
            if ordering:
                return ordering

        return self.get_default_ordering(view)


    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)
        insensitive_ordering = getattr(view, 'ordering_case_insensitive_fields', ())

        if ordering:
            new_ordering = []
            for field in ordering:
                if field in insensitive_ordering:
                    new_ordering.append(Lower(field[1:]).desc() if field.startswith('-') else Lower(field).asc())
                else:
                    new_ordering.append(field)

            return queryset.order_by(*new_ordering)

        return queryset